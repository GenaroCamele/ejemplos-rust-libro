use pruebas::Operation;
use pruebas::{ConsData, Rdd, Tuple, VecOfTuples};
use rand::prelude::*;
use std::time::SystemTime;

/// Ejemplos de evaluacion completa del grafo de ejecucion
fn test_eval() {
    // +++++++++ PLAIN DATA +++++++++
    let vector: ConsData<usize> = Rdd::new_data_from_vec(vec![1, 2]);
    let a: VecOfTuples<usize> = vector.get_collect();
    println!("Plain Data -> {:?}", a);

    // +++++++++ MAP +++++++++
    let a_map_funcion: fn(Tuple<usize>) -> Tuple<(usize, usize, bool)> =
        |tuple| Tuple::new((tuple.0, 4, true));
    let vec_of_tuples: ConsData<usize> = Rdd::new_data_from_vec(vec![1, 2, 3]);
    let operation = Rdd::map(vec_of_tuples, a_map_funcion);

    let result: VecOfTuples<(usize, usize, bool)> = operation.get_collect();
    println!("Result -> {:?}", result); // Expected [(1, 4, true),(1, 4, true),(1, 4, true)]

    // Otro Map
    let a_map_funcion_2: fn(Tuple<(usize, usize, bool)>) -> Tuple<bool> =
        |tuple| Tuple::new((tuple.0).0 % 2 == 0);
    let operation_2 = Rdd::map(operation, a_map_funcion_2);
    let result = operation_2.get_collect();
    println!("Result -> {:?}", result); // Expected [false, true, false]

    // +++++++++ FILTER +++++++++
    let a_filter_function: fn(&Tuple<bool>) -> bool = |tuple| (*tuple).0;
    let operation_3 = Rdd::filter(operation_2, a_filter_function);
    let result = operation_3.get_collect();
    println!("Result -> {:?}", result); // Expected [true]

    // +++++++++ REDUCE +++++++++
    let vec_of_tuples_for_reduce: ConsData<usize> = Rdd::new_data_from_vec(vec![1, 2, 3]);
    let a_reduce_function: fn(usize, Tuple<usize>) -> usize = |acc, tuple| acc + tuple.0;
    let operation_4 = Rdd::reduce(vec_of_tuples_for_reduce, 0, a_reduce_function);
    let result = operation_4.get_collect();
    println!("Result -> {:?}", result); // Expected [6]

    // REDUCE CON CAMBIO DE TIPOS
    let vec_of_tuples_for_reduce_2 = Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7]);
    let a_reduce_function: fn(bool, Tuple<usize>) -> bool = |acc, tuple| acc && (tuple.0 > 1);
    let operation_5 = Rdd::reduce(vec_of_tuples_for_reduce_2, false, a_reduce_function);
    let result = operation_5.get_collect();
    println!("Result reduce cambio de tipo -> {:?}", result); // Expected [false]

    // Ejemplo de WordCount!
    let words = Rdd::new_data_from_vec(vec!["Hola", "mundo", "chau", "mundo"]);
    let add_1_function: fn(Tuple<&str>) -> Tuple<(&str, usize)> = |tuple| Tuple::new((tuple.0, 1));
    let map = Rdd::map(words, add_1_function);
    let resultado_map_1 = map.get_collect();
    println!(
        "Tuplas antes de hacer un WordCount -> {:?}",
        resultado_map_1
    ); // Expected [("Hola", 1), ("mundo", 1), ("chau", 1), ("mundo", 1)]

    // Especifico una funcion para definir claves valores
    let set_key: fn(Tuple<(&str, usize)>) -> (&str, usize) = |tuple| {
        let values = tuple.0;
        (values.0, values.1)
    };

    // Seteo la funcion de reduccion
    let reduce_function: fn(usize, usize) -> usize = |acc, tuple| acc + tuple;
    let reduce_by_key_op = Rdd::reduce_by_key(map, set_key, 0, reduce_function);

    let result_wordcount = reduce_by_key_op.get_collect();
    println!("WordCount! -> {:?}", result_wordcount);

    // Ejemplo de estimacion del numero PI
    // Ver equivalente en Spark en https://spark.apache.org/examples.html
    let inside: fn(&Tuple<usize>) -> bool = |_| {
        let mut rng = rand::thread_rng();
        let (x, y): (f64, f64) = (rng.gen(), rng.gen());
        x * x + y * y < 1.0
    };

    let num_samples = 100000;
    let range: Vec<usize> = (0..num_samples).collect();
    let range_tuples: ConsData<usize> = Rdd::new_data_from_vec(range);

    let filter = Rdd::filter(range_tuples, inside);
    let count = filter.get_count();
    println!(
        "Pi is roughly {}",
        (4.0 * count as f64 / num_samples as f64)
    );

    // +++++++++ CARTESIAN +++++++++
    let cartesian = Rdd::cartesian(
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5]),
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5]),
    );

    println!("Resultado Cartesian -> {:?}", cartesian.get_collect());
}

/// Ejemplos de contabilidad de tuplas del grafo de ejecucion
fn test_count() {
    // +++++++++ MAP +++++++++
    let a_map_funcion: fn(Tuple<usize>) -> Tuple<(usize, usize, bool)> = |tuple| {
        println!("Esto no deberia imprirse ni una vez!");
        Tuple::new((tuple.0, 4, true))
    };
    let vec_of_numbers: Vec<usize> = (0..100).collect();
    let vec_of_tuples = Rdd::new_data_from_vec(vec_of_numbers);
    let operation = Rdd::map(vec_of_tuples, a_map_funcion);
    let result = operation.get_count();
    println!("Count del map -> {:?}", result); // Expected 100

    // COUNT CARTESIANO COMPLEJO
    let cartesian = Rdd::cartesian(
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    let complex_cartesian = Rdd::cartesian(
        cartesian,
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    let ultra_complex_cartesian = Rdd::cartesian(
        complex_cartesian,
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    println!(
        "Resultado Count Cartesian -> {:?}",
        ultra_complex_cartesian.get_count()
    ); // Expected 10 * 10 * 10 * 10 = 10000
}

/// Genera pruebas de los metodos que hacen uso del lazyness de los iteradores
/// como por ejemplo el first o el take
fn test_lazy() {
    // First sobre un map largo
    let a_map_funcion: fn(Tuple<usize>) -> Tuple<(usize, usize, bool)> =
        |tuple| Tuple::new((tuple.0, 4, true));
    let vec_of_tuples: ConsData<usize> = Rdd::new_data_from_vec((0..1000000).collect());
    let operation = Rdd::map(vec_of_tuples, a_map_funcion);

    let first: VecOfTuples<(usize, usize, bool)> = operation.first();
    println!("First sobre un map largo -> {:?}", first);


    let cartesian = Rdd::cartesian(
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    let complex_cartesian = Rdd::cartesian(
        cartesian,
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    let ultra_complex_cartesian = Rdd::cartesian(
        complex_cartesian,
        Rdd::new_data_from_vec(vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
    );

    let extremely_ultra_complex_cartesian = Rdd::cartesian(
        ultra_complex_cartesian,
        Rdd::new_data_from_vec((0..10000).collect()),
    );

    // println!("Todos los elementos del cartesiano -> {:?}", extremely_ultra_complex_cartesian.get_collect()); // OJO! Esta linea tarda varios minutos en terminar
    let sys_time = SystemTime::now();
    let count = extremely_ultra_complex_cartesian.get_count();
    let difference = sys_time.elapsed();
    println!("Cantidad de elementos -> {}! Calculado en {:?}", count, difference);

    let sys_time_2 = SystemTime::now();
    let first = extremely_ultra_complex_cartesian.first();
    let difference = sys_time_2.elapsed();
    println!("Un solo resultado del cartesiano complejo -> {:?}! Calculado en {:?}", first, difference);

    let sys_time_3 = SystemTime::now();
    let top_3 = extremely_ultra_complex_cartesian.top(3);
    let difference_3 = sys_time_3.elapsed();
    println!("Solo 3 elementos del resultado del cartesiano complejo -> {:?}! Calculado en {:?}", top_3, difference_3);
}

fn main() {
    test_eval();
    test_count();
    test_lazy();
}
