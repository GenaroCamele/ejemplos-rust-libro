// TODO: probar con RC para poder pasar los iteradores a traves de los metodos
// Por el momento solo se puede hacer un grafo sin reutilizaciones o bifurcaciones

// TODO: Paralelizar con Rayon
use std::collections::HashMap;
use std::hash::Hash;
use std::iter::{Filter, Map};
#[macro_use]
extern crate itertools;

// Heterogeneous Tuple
#[derive(Debug, Clone)]
pub struct Tuple<T: ?Sized>(pub T);

impl<T> Tuple<T> {
    pub fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

pub struct Rdd;

impl Rdd {
    /// Conviete un arreglo de cualquier tipo de datos a un arreglo de tuplas
    /// que podra ser utilizada para las operaciones
    pub fn new_data_from_vec<T>(data_vector: Vec<T>) -> ConsData<T> {
        let mut final_data: VecOfTuples<T> = Vec::with_capacity(data_vector.len());

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        ConsData {
            data: final_data.into_iter(),
        }
    }

    /// Adds a Map function
    pub fn map<O, U, T>(data: O, map_function: fn(Tuple<U>) -> Tuple<T>) -> MapOperation<O, U, T> {
        MapOperation { data, map_function }
    }

    /// Adds a Filter function
    pub fn filter<O, U>(data: O, filter_function: fn(&Tuple<U>) -> bool) -> FilterOperation<O, U> {
        FilterOperation {
            data,
            filter_function,
        }
    }

    /// Adds a Reduce function
    pub fn reduce<O, T, U>(
        data: O,
        init_value: U,
        reduce_function: fn(U, Tuple<T>) -> U,
    ) -> ReduceOperation<O, T, U> {
        ReduceOperation {
            data,
            init_value,
            reduce_function,
        }
    }

    /// Adds a Cartesian function
    pub fn cartesian<O, U>(data_1: O, data_2: U) -> CartesianOperation<O, U> {
        CartesianOperation { data_1, data_2 }
    }

    /// Adds a ReduceByKey function
    pub fn reduce_by_key<O, T, U, K, O2>(
        data: O,
        transform_function: fn(Tuple<T>) -> (K, O2),
        init_value: U,
        reduce_function: fn(U, O2) -> U,
    ) -> ReduceByKeyOperation<O, T, U, K, O2> {
        ReduceByKeyOperation {
            data,
            transform_function,
            init_value,
            reduce_function,
        }
    }

    /// Convierte una tupla comun en una Tupla de clave valor para poder operar con funciones como ReduceByKey
    pub fn tuple_cons_to_pair_data<K, T, T2>(
        cons_data: Tuple<T>,
        transform_function: fn(Tuple<T>) -> (K, T2),
    ) -> (Tuple<K>, Tuple<T2>) {
        let transformed: (K, T2) = transform_function(cons_data);
        (Tuple::new(transformed.0), Tuple::new(transformed.1))
    }
}

// Tipo de dato para simpleza
pub type VecOfTuples<T> = Vec<Tuple<T>>;
// type PairData<K, T> = Vec<(Tuple<K>, Tuple<T>)>;

pub trait Operation<'a, T: 'a>
where
    T: Clone,
{
    type MyIter: Iterator<Item = Tuple<T>>;

    /// Return the count of elements
    fn get_count(&'a self) -> usize;

    /// Generates the final iterator with all the operations specified in the graph of execution
    fn eval(&'a self) -> Self::MyIter;

    /// Get all the resulting elements of the graph execution
    fn get_collect(&'a self) -> VecOfTuples<T> {
        self.eval().collect()
    }

    /// Returns only the first element
    fn first(&'a self) -> VecOfTuples<T> {
        self.top(1)
    }

    /// Returns N elements of the iterator
    fn top(&'a self, n: usize) -> VecOfTuples<T> {
        self.eval().take(n).collect()
    }
}

// Constant Data!
pub struct ConsData<T> {
    data: std::vec::IntoIter<Tuple<T>>,
}

// Operation implementation for ConsData
impl<'a, T: 'a> Operation<'a, T> for ConsData<T>
where
    T: Clone,
{
    type MyIter = std::vec::IntoIter<Tuple<T>>;

    fn eval(&'a self) -> Self::MyIter {
        self.data.clone()
    }

    fn get_count(&self) -> usize {
        self.data.len()
    }
}

// Implementation for Map
pub struct MapOperation<O, U, T> {
    data: O,
    map_function: fn(Tuple<U>) -> Tuple<T>,
}

impl<'a, O, T, U> Operation<'a, T> for MapOperation<O, U, T>
where
    O: Operation<'a, U>, // Mi parametro O va a devolver un iterador sobre un tipo U
    U: 'a + Clone,       // El tipo U que devuelve mi parametro O es clonable tambien
    T: 'a + Clone,       // Yo voy a devolver un iterador de tipo T que es clonable
{
    type MyIter = Map<<O as Operation<'a, U>>::MyIter, fn(Tuple<U>) -> Tuple<T>>;

    fn eval(&'a self) -> Self::MyIter {
        self.data.eval().map(self.map_function) /* .map(self.map_function) */
    }

    fn get_count(&'a self) -> usize {
        self.data.get_count()
    }
}

pub struct FilterOperation<O, U> {
    data: O,
    filter_function: fn(&Tuple<U>) -> bool,
}

impl<'a, O, T> Operation<'a, T> for FilterOperation<O, T>
where
    O: Operation<'a, T>,
    T: 'a + Clone,
{
    type MyIter = Filter<<O as Operation<'a, T>>::MyIter, for<'r> fn(&'r Tuple<T>) -> bool>;
    fn eval(&'a self) -> Self::MyIter {
        self.data.eval().filter(self.filter_function)
    }

    fn get_count(&'a self) -> usize {
        // In the Filter case we don't know how many Tuples will be filtered
        self.eval().count()
    }
}

pub struct ReduceOperation<O, T, U> {
    data: O,
    init_value: U,
    reduce_function: fn(U, Tuple<T>) -> U,
}

impl<'a, O, T, U> Operation<'a, U> for ReduceOperation<O, T, U>
where
    O: Operation<'a, T>,
    T: 'a + Clone,
    U: 'a + Copy,
{
    type MyIter = std::vec::IntoIter<Tuple<U>>;

    fn eval(&'a self) -> Self::MyIter {
        let fold_result = self.data.eval().fold(self.init_value, self.reduce_function);
        let mut result: VecOfTuples<U> = Vec::with_capacity(1);
        result.push(Tuple::new(fold_result));
        result.into_iter()
    }

    fn get_count(&'a self) -> usize {
        1
    }
}

// REDUCE BY KEY
pub struct ReduceByKeyOperation<O, T, U, K, O2> {
    data: O,
    transform_function: fn(Tuple<T>) -> (K, O2),
    init_value: U,
    reduce_function: fn(U, O2) -> U,
}

impl<'a, O, T, U, K, O2> Operation<'a, (K, U)> for ReduceByKeyOperation<O, T, U, K, O2>
where
    O: Operation<'a, T>,
    K: 'a + Eq + Hash + Clone,
    T: 'a + Copy + Hash + Eq,
    U: 'a + Copy,
{
    type MyIter = std::vec::IntoIter<Tuple<(K, U)>>;

    fn eval(&'a self) -> Self::MyIter {
        // Genero un HashMap con la clave y el valor
        let mut result_by_key = HashMap::new();
        let data_eval_iterator = self.data.eval();
        let lenght_data = self.data.get_count();
        for tuple in data_eval_iterator {
            let pair_tuple = Rdd::tuple_cons_to_pair_data(tuple, self.transform_function);
            let key = (pair_tuple.0).0;
            let values = (pair_tuple.1).0;
            let values_for_tuple_key = result_by_key.entry(key).or_insert(vec![]);
            values_for_tuple_key.push(values);
        }

        let mut result: VecOfTuples<(K, U)> = Vec::with_capacity(lenght_data);
        for (key, values) in result_by_key {
            let reduced_values = values
                .into_iter()
                .fold(self.init_value, self.reduce_function);
            result.push(Tuple::new((key, reduced_values)));
        }
        result.into_iter()
    }

    fn get_count(&'a self) -> usize {
        // TODO: aca deberia ser un countByKey. Cuando se implemente
        self.eval().count()
    }
}

pub struct CartesianOperation<O, U> {
    data_1: O,
    data_2: U,
}

impl<'a, O, U, T, H> Operation<'a, (T, H)> for CartesianOperation<O, U>
where
    O: Operation<'a, T>,
    H: 'a + Clone,
    T: 'a + Clone + Copy,
    U: Operation<'a, H>,
    <U as Operation<'a, H>>::MyIter: Clone,
{
    type MyIter = Map<
        itertools::Product<<O as Operation<'a, T>>::MyIter, <U as Operation<'a, H>>::MyIter>,
        fn((Tuple<T>, Tuple<H>)) -> Tuple<(T, H)>,
    >;

    fn eval(&'a self) -> Self::MyIter {
        let eval_1 = self.data_1.eval();
        let eval_2 = self.data_2.eval();
        // Define a tuple convertion
        let tuple_convertion: fn((Tuple<T>, Tuple<H>)) -> Tuple<(T, H)> =
            |(tuple_1, tuple_2)| Tuple::new((tuple_1.0, tuple_2.0));
        iproduct!(eval_1, eval_2).map(tuple_convertion)
    }

    fn get_count(&'a self) -> usize {
        self.data_1.get_count() * self.data_2.get_count()
    }
}
