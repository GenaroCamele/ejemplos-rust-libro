use std::collections::HashMap;
use std::hash::Hash;
#[macro_use]
extern crate itertools;

// Tuplas sin tamaño fijo
#[derive(Debug, Clone)]
pub struct Tuple<T: ?Sized>(pub T);

// Crea un nuevo data
impl<T> Tuple<T> {
    pub fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Tipo de dato para simpleza
type ConsData<T> = Vec<Tuple<T>>;
type PairData<K, T> = Vec<(Tuple<K>, Tuple<T>)>;

// Implemento un trait para que cambie el tipo del vec
pub trait Operation<T> {
    fn eval(&self) -> ConsData<T>;
    fn count(&self) -> usize;
}

pub struct Rdd;

impl Rdd {
    /// Conviete un arreglo de cualquier tipo de datos a un arreglo de tuplas
    /// que podra ser utilizada para las operaciones
    pub fn new_data_from_vec<T>(data_vector: Vec<T>) -> ConsData<T> {
        let mut final_data: ConsData<T> = Vec::with_capacity(data_vector.len());

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        final_data
    }

    /// Genera una funcion Map
    pub fn map<O, U, T>(data: O, map_function: fn(Tuple<U>) -> Tuple<T>) -> MapOperation<O, U, T> {
        MapOperation { data, map_function }
    }

    /// Genera una funcion Filter
    pub fn filter<O, U>(data: O, filter_function: fn(&Tuple<U>) -> bool) -> FilterOperation<O, U> {
        FilterOperation {
            data,
            filter_function,
        }
    }

    /// Genera una funcion Reduce
    pub fn reduce<O, T, U>(
        data: O,
        init_value: U,
        reduce_function: fn(U, Tuple<T>) -> U,
    ) -> ReduceOperation<O, T, U> {
        ReduceOperation {
            data,
            init_value,
            reduce_function,
        }
    }
    /// Genera una funcion ReduceByKey
    pub fn reduce_by_key<O, T, U, K, O2>(
        data: O,
        transform_function: fn(Tuple<T>) -> (K, O2),
        init_value: U,
        reduce_function: fn(U, O2) -> U,
    ) -> ReduceByKeyOperation<O, T, U, K, O2> {
        ReduceByKeyOperation {
            data,
            transform_function,
            init_value,
            reduce_function,
        }
    }

    /// Genera una funcion Cartesian
    pub fn cartesian<O, U>(data_1: O, data_2: U) -> CartesianOperation<O, U> {
        CartesianOperation { data_1, data_2 }
    }

    /// Convierte un rdd constante al tipo de rdd clave valor para poder operar con funciones
    /// como ReduceByKey
    pub fn _cons_to_pair_data<K, T, T2>(
        cons_data: ConsData<T>,
        transform_function: fn(Tuple<T>) -> (K, T2),
    ) -> PairData<K, T2> {
        let mut result: PairData<K, T2> = Vec::with_capacity(cons_data.len());
        for tuple in cons_data.into_iter() {
            let transformed: (K, T2) = transform_function(tuple);
            result.push((Tuple::new(transformed.0), Tuple::new(transformed.1)));
        }
        result
    }

    /// Convierte una tupla comun en una Tupla de clave valor para poder operar con funciones como ReduceByKey
    pub fn tuple_cons_to_pair_data<K, T, T2>(
        cons_data: Tuple<T>,
        transform_function: fn(Tuple<T>) -> (K, T2),
    ) -> (Tuple<K>, Tuple<T2>) {
        let transformed: (K, T2) = transform_function(cons_data);
        (Tuple::new(transformed.0), Tuple::new(transformed.1))
    }
}

// Implemento la operacion para la data constante
impl<T: Clone> Operation<T> for ConsData<T> {
    fn eval(&self) -> ConsData<T> {
        self.clone()
    }

    fn count(&self) -> usize {
        self.len()
    }
}

// MAP
pub struct MapOperation<O, U, T> {
    data: O,
    map_function: fn(Tuple<U>) -> Tuple<T>,
}

impl<O, T, U> Operation<T> for MapOperation<O, U, T>
where
    O: Operation<U>,
{
    fn eval(&self) -> ConsData<T> {
        self.data
            .eval()
            .into_iter()
            .map(self.map_function)
            .collect()
    }

    fn count(&self) -> usize {
        self.data.count()
    }
}

pub struct CartesianOperation<O, U> {
    data_1: O,
    data_2: U,
}

impl<O, U, T, H> Operation<(T, H)> for CartesianOperation<O, U>
where
    O: Operation<T>,
    // Tuple<T>: Clone,
    H: Clone,
    T: Clone + Copy,
    U: Operation<H>,
{
    fn eval(&self) -> ConsData<(T, H)> {
        let eval_1 = self.data_1.eval();
        let eval_2 = self.data_2.eval();
        let mut result: ConsData<(T, H)> = Vec::with_capacity(eval_1.len() * eval_2.len());
        for (tuple_1, tuple_2) in iproduct!(eval_1, eval_2) {
            result.push(Tuple::new((tuple_1.0, tuple_2.0)));
        }
        result
    }

    fn count(&self) -> usize {
        self.data_1.count() * self.data_2.count()
    }
}

// FILTER
pub struct FilterOperation<O, U> {
    data: O,
    filter_function: fn(&Tuple<U>) -> bool,
}

impl<O, T> Operation<T> for FilterOperation<O, T>
where
    O: Operation<T>,
    ConsData<T>: Operation<T>,
{
    fn eval(&self) -> ConsData<T> {
        self.data
            .eval()
            .into_iter()
            .filter(self.filter_function)
            .collect()
    }

    fn count(&self) -> usize {
        // En el caso de filter necesito evaluar el grafo de ejecucion
        // porque no se de antemano cuantas tuplas se filtraran
        self.eval().count()
    }
}

// REDUCE
pub struct ReduceOperation<O, T, U> {
    data: O,
    init_value: U,
    reduce_function: fn(U, Tuple<T>) -> U,
}

impl<O, T, U> Operation<U> for ReduceOperation<O, T, U>
where
    O: Operation<T>,
    T: Copy,
    U: Copy,
{
    fn eval(&self) -> ConsData<U> {
        let fold_result = self
            .data
            .eval()
            .into_iter()
            .fold(self.init_value, self.reduce_function);
        vec![Tuple::new(fold_result)]
    }

    fn count(&self) -> usize {
        1
    }
}

// REDUCE BY KEY
pub struct ReduceByKeyOperation<O, T, U, K, O2> {
    data: O,
    transform_function: fn(Tuple<T>) -> (K, O2),
    init_value: U,
    reduce_function: fn(U, O2) -> U,
}

impl<O, T, U, K, O2> Operation<(K, U)> for ReduceByKeyOperation<O, T, U, K, O2>
where
    O: Operation<T>,
    K: Eq + Hash,
    T: Copy + Hash + Eq,
    U: Copy,
    ConsData<(K, U)>: Operation<T>,
{
    fn eval(&self) -> ConsData<(K, U)> {
        // Genero un HashMap con la clave y el valor
        let mut result_by_key = HashMap::new();
        let data_eval = self.data.eval();
        let lenght_data = data_eval.len();
        for tuple in data_eval.into_iter() {
            let pair_tuple = Rdd::tuple_cons_to_pair_data(tuple, self.transform_function);
            let key = (pair_tuple.0).0;
            let values = (pair_tuple.1).0;
            let values_for_tuple_key = result_by_key.entry(key).or_insert(vec![]);
            values_for_tuple_key.push(values);
        }

        let mut result: ConsData<(K, U)> = Vec::with_capacity(lenght_data);
        for (key, values) in result_by_key {
            let reduced_values = values
                .into_iter()
                .fold(self.init_value, self.reduce_function);
            result.push(Tuple::new((key, reduced_values)));
        }
        result
    }

    fn count(&self) -> usize {
        self.eval().count()
    }
}
