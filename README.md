# Ejemplos para correr

Todos los scripts se encuentran en la carpeta `examples`. Para correrlos ejecutar

`cargo run --example <nombre del archivo sin el .rs>`

De esta manera se descargan las dependencias, se puede correr en modo debug o release y además los errores en consola aparecen con colores.

Si se quiere correr y que arroje el backtrace en pantalla:

`RUST_BACKTRACE=1 cargo run --example <nombre del archivo sin el .rs>`

O setear directamente la variable de entorno para no tener que poner ese prefijo siempre.