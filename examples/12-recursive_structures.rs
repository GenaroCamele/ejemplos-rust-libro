
enum List<T> {
    Nil,
    Cons(T, Box<List<T>>)
}

// Cuenta la cantidad de elementos que hay en la lista
fn count_layers_in_structure<T>(list: List<T>) -> u32 {
    match list {
        List::Nil => 0,
        List::Cons(_, e1) => 1 + count_layers_in_structure(*e1)
    }
}

fn main() {
    let list = List::Cons(
        32,
        Box::new(List::Cons(
            55,
            Box::new(List::Cons(
                2,
                Box::new(List::Nil)
            ))
        ))
    );

    let count_layers = count_layers_in_structure(list);
    println!("La cantidad de elementos en la lista es -> {}", count_layers);

    match list {
        List::Cons(a, _) => println!("Accedo a los elementos! {}", a),
        _ => println!("Cualquier cosa"),
    }
}