struct MySuperSliceable<T: ?Sized> {
    info: u32,
    data: T,
}

#[derive(Debug, Clone)]
struct Data<T: ?Sized>(T);

impl<T> Data<T> {
    fn new(x: T) -> Data<T> {
        Data(x)
    }
}

#[derive(Debug, Clone)]
enum Result<T, H> {
    Res1(Data<H>),
    Res2(Data<T>)
}

fn modify_data<'a, T, H>(tuple: &'a Data<T>, function: fn(&'a Data<T>) -> Data<H>, function_2: fn(&'a Data<T>) -> Data<T>, change_type: bool) -> Result<T, H>
{
    if change_type {
        Result::Res1(function(&tuple))
    } else {
        Result::Res2(function_2(&tuple))
    }
}

fn main() {
    let sized: MySuperSliceable<[u8; 8]> = MySuperSliceable {
        info: 17,
        data: [0; 8],
    };

    let dynamic: &MySuperSliceable<[u8]> = &sized;

    // prints: "17 [0, 0, 0, 0, 0, 0, 0, 0]"
    println!("{} {:?}", dynamic.info, &dynamic.data);

    let datos = Data::new((1, 2, true, 4, 'a',6));
    println!("Datos -> {:?}", datos);
    
    // let datos_2 = Data::new((String::from("Hola"), 22323, true, -12, 'a', String::from("5")));
    let datos_2 = Data::new((1, 2, true, 323, 'a', 5));
    println!("Datos -> {:?}", (datos_2.0).3);

    let mut vec_1/* : Vec<Data<_>> */ = Vec::new();
    vec_1.push(datos);
    vec_1.push(datos_2);
    let vec_2: Vec<(_, _)> = vec_1.iter().map(|tuple| ((tuple.0).0, (tuple.0).3)).collect();
    for tupla in vec_2.iter() {
        println!("Tupla modificada -> {:?}", tupla);
    }
    
    println!("Llamando a una funcion con modificacion de tupla");
    let function_1 = |_| Data::new((true, 64));
    let function_2 = |_| Data::new((12, 23, true, 323, 'a', 5));
    let data_modified = modify_data(&vec_1[0], function_1, function_2, true);
    println!("Data modificada -> {:?}", data_modified);


    let data_modified = modify_data(&vec_1[0], function_1, function_2, false);
    println!("Data modificada -> {:?}", data_modified);
}