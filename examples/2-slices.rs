fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }

    return s.len();
}

fn first_word_with_slice(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..] // Lo mismo que hacer &s o &s[0..s.len()]
}

fn first_word_with_slice_with_str(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..] // Lo mismo que hacer &s o &s[0..s.len()]
}

fn main() {
    let s1 = String::from("Holaa como andas?");
    let f1 = first_word(&s1);
    println!("{}", f1);

    let s1 = String::from("Holaa como andas?");
    let f1 = first_word_with_slice(&s1);
    println!("{}", f1);

    let s2 = String::from("adjaskjdkas");
    let f2 = first_word_with_slice(&s2);
    println!("{}", f2);

    // Puedo usar los Strings como &str
    let s3 = String::from("String objeto para &str");
    let f3 = first_word_with_slice_with_str(&s3);
    println!("{}", f3);

    let s4 = "String literal";
    let f4 = first_word_with_slice_with_str(&s4); // Como referencia
    let f5 = first_word_with_slice_with_str(s4); // Sin referencia
    println!("{}", f4);
    println!("{}", f5);
}