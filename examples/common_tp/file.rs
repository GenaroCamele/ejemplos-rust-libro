use std::io::{self, Read};
use std::fs::File;

// Lee el contenido de un archivo y lo devuelve
pub fn read_file() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("/home/genaro/Proyectos/pruebasRust/examples/test.txt")?.read_to_string(&mut s)?;
    Ok(s) // Si llega aca entonces la linea de arriba no hizo el return de error
}

// Recibe el contenido de un archivo y lo devuelve flattened
pub fn get_flattened(content: String) -> Vec<(String)> {
    let content = content.lines().collect::<Vec<&str>>();
    let flattened_ref = content
        .iter()
        .flat_map(|line| line.split_whitespace())
        .collect::<Vec<&str>>();
    flattened_ref.iter().map(|word| word.to_string()).collect()
}