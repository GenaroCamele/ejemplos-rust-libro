use std::rc::Rc;

enum List<T, F = fn(T) -> (), H = ()>
{
    Nil,
    Cons(T, Rc<List<T, H>>),
    Map(F, Rc<List<H, H>>)
}

// Especifico F como una funcion que toma T
// Y a H como una tupla vacia () ó tambien llamada Unit
// Ambos parametros dummy para que no joda con el tamaño de los
// parametros en tiempo de ejecucion
impl<T> List<T, fn(T), ()> {
    fn new_data(data: T) -> Self {
        List::Cons(
            data,
            Rc::new(List::Nil)
        )
    }
}

// Cuenta la cantidad de elementos que hay en la lista
fn count_layers_in_structure<T, F, H>(list: &List<T, F, H>) -> u32 {
    match list {
        List::Nil => 0,
        List::Cons(_, e1) => 1 + count_layers_in_structure(&(*e1)),
        List::Map(_, e1) => 1 + count_layers_in_structure(&(*e1)),
    }
}

fn main() {
    let list = List::new_data(32);
    // let list = List::Cons(
    //     32,
    //     Rc::new(List::Cons(
    //         55,
    //         Rc::new(List::Cons(
    //             2,
    //             Rc::new(List::Nil)
    //         ))
    //     ))
    // );

    let count_layers = count_layers_in_structure(&list);
    println!("La cantidad de elementos en la lista es -> {}", count_layers);
}