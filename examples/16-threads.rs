use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..3 {
            println!("Soy el thread DISPARADO y voy por el {}", i);
            thread::sleep(Duration::from_millis(1000));
        }
    });


    for i in 1..5 {
        println!("Soy el PRINCIPAL y voy por el {}", i);
        thread::sleep(Duration::from_millis(1));
    }

    // Con la siguiente linea se espera a que termine el thread
    handle.join().unwrap();

    // Si queremos que el thread pueda acceder a las variables del contexto
    // se debe utilizar la keyword "move" para transferir el owwnership
    // de las variables que se utilizen automaticamente
    let x = String::from("Hola");
    let y = 10;
    let handle_thread_2 = thread::spawn(move || {
        println!("Soy el thread y puedo leer x -> {}", x);
        println!("Soy el thread y puedo leer y -> {}", y);
    });

    // OJO: recordar que toma el ownership
    // println!("x -> {}", x); // Error, x ya fue pasado al thread
    println!("y -> {}", y); // Con los primitivos no hay drama porque se alojan en la stack y se copian, no se toman
    
    handle_thread_2.join().unwrap();
}