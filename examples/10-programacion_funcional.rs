use rand;
use std::collections::HashMap;

// Funcion que toma como parametro una funcion que toma como parametro un numero 
// y lo imprime
fn call_some_function<T>(some_funcion: T)
    where T: Fn(u32)
{
    println!("Llamando a la funcion que me mandaron por parámetro con algun número random");
    some_funcion(rand::random());
}

fn main() {
    // Funciones anonimas
    // Los {} son opcionales si las fn anon. son de una unica linea
    let print_some_number_fn = |some_number| {
        println!("Me mandaron {}", some_number);
    };

    // Llamamos a una funcion y le damos el closure declarado arriba
    call_some_function(print_some_number_fn);

    // Instancio un cacher con un closure
    let mut catcher = Cacher::new(|x| x + 5);
    println!("El valor es -> {}", catcher.value(8)); // 13
    println!("El valor es -> {}", catcher.value(126)); // PROBLEMA: El resultado es el mismo que la linea de arriba -> 13. Porque esta cacheado

    // Creamos un cacher sin el problema de arriba
    println!("\n\nCon Cacher mejorado!");
    let mut catcher_hash = CacherHash::new(|x| x + 5);
    println!("El valor es -> {}", catcher_hash.value(8)); // 13
    println!("El valor es -> {}", catcher_hash.value(126)); // 131
    println!("El valor es -> {}", catcher_hash.value(126)); // 131 y ya no deberia imprimir que se esta calculando el valor

    // Los closures piden prestadas las variables que lo rodean
    let x = 5;
    let cl = | y | y + x;
    println!("Resultado del llamado -> {}", cl(4)); // 9
    println!("Valor de X -> {} ", x);

    // Con la sentencia "move", el closure toma pertenencia de las variables externas
    // que utiliza
    let h = vec![1, 2, 3];
    let cl_ownership = move | z | z == h;
    println!("Resultado del llamado -> {}", cl_ownership(vec![1, 3, 5])); // false
    // println!("Valor de X -> {:#?} ", h); // ERROR! h ya perdio su ownership porque se "movio" al closure cl_ownership
}

// Esta estructura respeta el patrón de diseño llamado "memoization" or "lazy evaluation"
// Ya que almacena el resultado de su evaluacion para que no tenga que evaluarse mas de una vez
struct Cacher<T>
    where T: Fn(u32) -> u32
{
    calculation: T,
    value: Option<u32>
}

impl<T> Cacher<T>
    where T: Fn(u32) -> u32
{
    fn new(calculation: T) -> Self {
        Cacher {
            calculation,
            value: None
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.value {
            Some(value) => value,
            None => {
                println!("Calculando el valor!");
                // Llamo a la funcion que me pasaron por parametro
                let value = (self.calculation)(arg);
                self.value = Some(value);
                value
            }
        }
    }
}

// Version del cacher en el que pueden cambiar los parametros enviados
struct CacherHash<T>
    where T: Fn(u32) -> u32
{
    calculation: T,
    values: HashMap<u32, u32>
}

impl<T> CacherHash<T>
    where T: Fn(u32) -> u32
{
    fn new(calculation: T) -> Self {
        CacherHash {
            calculation,
            values: HashMap::new()
        }
    }

    fn value(&mut self, arg: u32) -> u32 {
        match self.values.get(&arg) {
            Some(value) => *value,
            None => {
                println!("Calculando el valor!");
                // Llamo a la funcion que me pasaron por parametro
                let value = (self.calculation)(arg);
                self.values.insert(arg, value);
                value
            }
        }
    }
}