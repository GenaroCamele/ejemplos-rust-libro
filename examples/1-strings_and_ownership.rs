fn main() {
    // Sin problemas de ownership (string literal que se copia en tiempo de compilacion)
    let a = "String literal";
    let b = a;
    println!("{}", a);
    println!("{}", b);
    
    // Con problemas de ownership
    let s1 = String::from("String objeto!");
    let s2 = s1;
    // println!("{}", s1); // ERROR: S1 ya no tiene la pertenencia del string
    println!("{}", s2);
    
    // Solucionar problema: copiar el valor completo
    let s3 = s2.clone();
    println!("{}", s2); // No sale error! 
    println!("{}", s3); // No sale error!

    // NOTA: estos errores de ownership solo ocurren con tipos que se alojan en la heap
    // como los arrays dinamicos o strings (que a fin de cuentas son lo mismo).
    // Los tipos que tienen tamaño fijo y se alojan en la pila (stack) no tienen este problema
    // ya que copiarlos es muy rápido
    // EJEMPLOS con otros tipos!
    let x = 30;
    let y = x;
    println!("x = {}, y = {}", x, y); // Sin problemas!

    // LOS LLAMADOS A LAS FUNCIONES SON EXACTAMENTE IGUALES A LAS ASIGNACIONES
    takes_ownership(s2);
    
    // println!("{}", s2); // Error! Ya no tengo la ownership de s2!
    println!("{}", s3); // Sin problemas! Es la COPIA de s2
    
    // Con los tipos en stack
    makes_copy(x);
    println!("x = {}, y = {}", x, y); // Sin problemas!
    
    // Si queremos pasar un string pero que no nos saquen el ownership hay
    // que poner un "&" en la definicion del parámetro de la función
    // y en la llamada
    not_takes_ownership(&s3);
    println!("{}", s3); // Sin problemas! La funcion no nos saco la pertenencia
    
    // Paso por referencia y encima mutable
    let mut s4 = String::from("Hola");
    not_takes_ownership_with_change(&mut s4);
    println!("Ahora deberia estar editado! {}", s4); // Sin problemas! La funcion no nos saco la pertenencia

    // Muestra de error de un puntero a una variable inexistente
    // let s_error = not_valid_dangle_reference();
}

// Usa el string y nos saca el ownership
fn takes_ownership(s: String) {
    println!("Parametro string recibido = {}", s);
}

// Usa el string y NO nos saca el ownership (usa la referencia)
// Esto se llama borrowing
fn not_takes_ownership(s: &String) {
    println!("Parametro string recibido por REFERENCIA = {}", s);
    
    // NOTA: cuando está por borrowing no se puede editar el valor
    // s.push_str("Pruebitaaa!"); // Error! No se puede editar
}

// Lo mismo que not_takes_ownership pero con la variable mutable
// es decir, le podemos cambiar el valor dentro de la funcion
// para ello se le agrega el &mut al tipo del parámetro
fn not_takes_ownership_with_change(s: &mut String) {
    s.push_str("Pruebitaaa!"); // Error! No se puede editar
}

// Cuando se llama a esta funcion, como es una variable en stack se hace la copia
fn makes_copy(x: i32) {
    println!("Parametro x recibido = {}", x);
}

// No puedo pasar punteros a variables que se liberan desde a memoria
// Comentada porque sino no compila
// fn not_valid_dangle_reference() -> &String {
//     let s = String::from("Prueba");
//     return &s; // ERRROR! s se libera al terminar la funcion, no puedo devolver su referencia
// }