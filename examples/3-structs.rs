// Struct
#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

// Metodos
impl User {
    fn say_hi(&self) {
        println!("Hola, mi username es {}", self.username);
    }

    // Cuando queremos editar algun valor de la instancia tenemos
    // que definir el parametro self como mutable
    fn toggle_active(&mut self) {
        self.active = !self.active;
    }

    // Funciones Asociadas! No toman self como parametro
    // Deben llamarse con el ::
    fn say_struct_name() {
        println!("Soy el struct de User")
    }

    // Se suele utilizar para crear constructores personalizados
    fn create(username: String, email: String) -> User {
        User {
            username,
            email,
            active: true,
            sign_in_count: 1,
        }
    }
}

// Tuple Struct
#[derive(Debug)]
struct Point(i32, i32, i32);

// Genera un usuario
fn build_user(username: String, email: String) -> User {
    // NOTA: si el key: valor son la misma palabra se puede poner una unica vez.
    // En este caso hacemos eso con username y email
    User {
        username,
        email,
        sign_in_count: 1,
        active: true,
    }
}

fn main() {
    // Instancia una struct
    let mut user1 = User {
        username: String::from("Genarito"),
        email: String::from("genaro@hotmail.com"),
        active: true,
        sign_in_count: 1,
    };

    println!("Email -> {}", user1.email);

    // Si lo especifique como mut, puedo cambiarle los campos
    user1.email = String::from("genareteee@hotmail.com");
    println!("Email -> {}", user1.email);

    let user2 = build_user(String::from("Otro"), String::from("otro@gmail.com"));

    // Creamos nuevas entidades sin/con update sintax
    // Es util cuando querés crear intancias con los mismos campos que otra instancia pero con
    // algunos cambios

    // SIN Update Sintax
    let user3 = User {
        username: String::from("Usuario 3"),
        email: String::from("Usuario@gmail.com"),
        active: user1.active,
        sign_in_count: user1.sign_in_count,
    };

    // CON Update Sintax
    let user4 = User {
        username: String::from("Usuario 3"),
        email: String::from("Usuario@gmail.com"),
        ..user1
    };

    // Instancio una estructura de tupla.
    // NOTA: Para acceder a los campos se debe hacer como si fueran atributos, no indices.
    // Por eso se accede con p1.x y no con p1[x]
    let p1 = Point(22, 33, 11);
    println!("X -> {} | Y -> {} | Z -> {}", p1.0, p1.1, p1.2);

    // Imprimir estructuras
    // En el formatting hay que agregar :#? (mas legible) o :? (en una sola linea) para que se impriman.
    // Previo a eso, agregar #[derive(Debug)] arriba de cada struct que nos interese imprimir
    println!("{:?}", user1);
    println!("{:#?}", p1);

    // Uso de metodos
    user1.say_hi();

    // Usando metodos que cambian los atributos de la instancia
    println!(
        "El usuario {} está {}",
        user1.username,
        if user1.active {
            "Activado"
        } else {
            "Desactivado"
        }
    );
    user1.toggle_active();
    println!(
        "El usuario {} está {}",
        user1.username,
        if user1.active {
            "Activado"
        } else {
            "Desactivado"
        }
    );

    // Uso las funciones asociadas!
    User::say_struct_name();
    let user5 = User::create(String::from("Genarete"), String::from("camele@gmail.com"));
}
