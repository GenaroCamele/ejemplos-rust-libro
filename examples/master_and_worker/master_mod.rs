pub struct MasterMod {
    pub ip: String, // Dentro de los modulos, todos los campos de los structs son privados a menos que se indique lo contrario
    port: String // Este es privado
}

impl MasterMod {
    pub fn create_from(ip: String, port: String) -> Self {
        MasterMod {
            ip,
            port
        }
    }

    // Aca puedo acceder a port porque es una impl de la struct dentro del modulo
    pub fn print_info(&self) {
        println!("IP = {} | Port = {}", self.ip, self.port);
    }
}