// Defino el nombre del paquete
// mod master_and_worker;

// Debo definir publicamente lo que quiero que se exponga desde master_and_worker

// Exporto el Master
mod master_mod;
pub use self::master_mod::MasterMod;

// Exporto el Worker
mod worker_mod;
pub use self::worker_mod::WorkerMod;