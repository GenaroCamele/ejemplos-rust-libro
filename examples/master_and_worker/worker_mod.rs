pub struct WorkerMod {
    pub ip: String,
    port: String
}

impl WorkerMod {
    pub fn create_worker_from(ip: String, port: String) -> Self {
        WorkerMod {
            ip,
            port
        }
    }

    // Aca puedo acceder a port porque es una impl de la struct dentro del modulo
    pub fn print_info(&self) {
        println!("Soy un worker con IP = {} | Port = {}", self.ip, self.port);
    }
}