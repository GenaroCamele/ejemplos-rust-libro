// Hay dos tipos de errores: los recuperables y los no recuperables
// Para los primeros se usa Result
// Para los segundos se usa la macro panic!

// Aca se tratan los primeros (Result)

// Seteando la variable de entorno RUST_BACKTRACE=1 o RUST_BACKTRACE=full
// se puede obtener un backtrace completo de la ejecucion

use std::fs::File;
use std::io::{self, ErrorKind, Read};

// Chequea si un archivo existe haciendo Pattern Matching con
// el Result devuelto en la apertura
fn check_if_file_exists(filename: String) {
    let f = File::open(&filename);

    // NOTA: Para evitar todos los matchs anidados se puede utilizar closures! (Ver archivo closures.rs)
    match f {
        Ok(_) => println!("Archivo {} abierto correctamente!", filename),
        Err(error) => {
            println!(
                "Error al abrir el archivo {}. Matcheando el error para ser precisos:",
                filename
            );
            match error.kind() {
                ErrorKind::NotFound => println!("\t- El archivo no existe"),
                ErrorKind::PermissionDenied => {
                    println!("\t- No se tiene permiso para abrir el archivo")
                }
                other_error => println!("\t- Ocurrió otro error -> {:#?}", other_error),
            }
        }
    }
}

// Chequea si un archivo existe haciendo Pattern Matching con
// el Result devuelto en la apertura
fn check_if_file_exists_shortcuts(filename: String) {
    println!("Abriendo con shortcuts");

    // unwrap() devuelve el parametro de Ok() y en caso de error llama a panic!
    // File::open(&filename).unwrap();

    // expect() es lo mismo que unwrap() pero permite definir el mensaje de error en panic!
    File::open(&filename).expect(&format!("Error al abrir el archivo {}", filename));
    println!("\t - Archivo {} abierto con éxito!", filename);
}

// Funcion para devolver simplemente un Result y propagar
// el error de manera que lo pueda manejar quien llama
fn propagate_error(filename: String) -> Result<String, io::Error> {
    let f = File::open(filename);

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e), // Si falla devuelve el error de apertura
    };

    let mut s = String::new();

    // Lee hasta el primer string
    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}

// Funcion para devolver simplemente un Result y propagar
// el error de manera que lo pueda manejar quien llama utilizando
// el operador "?"
// El operador ? devuelve el parametro de Ok() en caso de exito o hace un return
// de Err(error)
fn propagate_error_with_shortcut() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    // Si llega aca no hizo el return de error en la apertura
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s) // Si llega aca entonces la linea de arriba no hizo el return de error
}

// Usa el operador ? pero en cadena
fn propagate_error_with_super_shortcut() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("hello.txt")?.read_to_string(&mut s)?;
    Ok(s) // Si llega aca entonces la linea de arriba no hizo el return de error
}

fn main() {
    // Tratamos de abrir un archivo que no existe
    check_if_file_exists(String::from("hello.txt"));
    // Ahora uno sin error
    check_if_file_exists(String::from("examples/test.txt"));

    // Tratamos de abrir un archivo que no existe con shortcuts
    // check_if_file_exists_shortcuts(String::from("hello.txt")); // Comentado para que no corte la ejecucion
    // Ahora uno sin error con shortcuts
    check_if_file_exists_shortcuts(String::from("examples/test.txt"));
    
    // Funcion que devuelve un Result para poder manejarlo externamente
    let res = propagate_error(String::from("examples/test.txt"));
    // Aca podria hacer match con res y otras cosas
    println!("Resultado de la propagación del error -> {:?}", res);
    
    let res_2 = propagate_error_with_shortcut();
    println!("Resultado de la propagación del error -> {:?}", res_2);
}
