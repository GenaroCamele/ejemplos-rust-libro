// TODO: agregar funciones al Rdd con los parametros genericos para comodidad a la hora de seguir
// agregando funciones al grafo

use std::collections::HashMap;
use std::hash::Hash;

// Tuplas sin tamaño fijo
#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Tipo de dato para simpleza
type ConsData<T> = Vec<Tuple<T>>;
type PairData<K, T> = Vec<(Tuple<K>, Tuple<T>)>;

// Implemento un trait para que cambie el tipo del vec
trait Operation<T> {
    fn eval(&self) -> ConsData<T>;
}

struct Rdd;

impl Rdd {
    /// Conviete un arreglo de cualquier tipo de datos a un arreglo de tuplas
    /// que podra ser utilizada para las operaciones
    fn new_data_from_vec<T>(data_vector: Vec<T>) -> ConsData<T> {
        let mut final_data: ConsData<T> = Vec::with_capacity(data_vector.len());

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        final_data
    }

    /// Convierte un rdd constante al tipo de rdd clave valor para poder operar con funciones
    /// como ReduceByKey
    fn _cons_to_pair_data<K, T, T2>(cons_data: ConsData<T>, transform_function: fn(Tuple<T>) -> (K, T2)) -> PairData<K, T2> {
        let mut result: PairData<K, T2> = Vec::with_capacity(cons_data.len());
        for tuple in cons_data.into_iter() {
            let transformed: (K, T2) = transform_function(tuple);
            result.push(
                (Tuple::new(transformed.0), Tuple::new(transformed.1))
            );
        }
    
        result
    }

    /// Convierte una tupla comun en una Tupla de clave valor para poder operar con funciones como ReduceByKey
    fn tuple_cons_to_pair_data<K, T, T2>(cons_data: Tuple<T>, transform_function: fn(Tuple<T>) -> (K, T2)) -> (Tuple<K>, Tuple<T2>) {
        let transformed: (K, T2) = transform_function(cons_data);
        (Tuple::new(transformed.0), Tuple::new(transformed.1))
    }
}

// Implemento la operacion para la data constante
impl<T: Clone> Operation<T> for ConsData<T> {
    fn eval(&self) -> ConsData<T> {
        self.clone()
    }
}

impl<K: Clone, T: Clone> Operation<T> for PairData<K, T> {
    fn eval(&self) -> ConsData<T> {
        // self.clone()
        Vec::new()
    }
}


// // Implemento la operacion para la data de clave/valoor
// impl<K: Clone, T: Clone> Operation<T> for PairData<K, T> {
//     fn eval(&self) -> ConsData<T> {
//         self.clone()
//     }
// }

// MAP
struct MapOperation<O, U, T> {
    data: O,
    map_function: fn(Tuple<U>) -> Tuple<T>,
}

impl<O, T, U> Operation<T> for MapOperation<O, U, T>
where
    O: Operation<U>,
{
    fn eval(&self) -> ConsData<T> {
        self.data
            .eval()
            .into_iter()
            .map(self.map_function)
            .collect()
    }
}

// FILTER
struct FilterOperation<O, U> {
    data: O,
    filter_function: fn(&Tuple<U>) -> bool,
}

impl<O, T> Operation<T> for FilterOperation<O, T>
where
    O: Operation<T>,
{
    fn eval(&self) -> ConsData<T> {
        self.data
            .eval()
            .into_iter()
            .filter(self.filter_function)
            .collect()
    }
}

// REDUCE
struct ReduceOperation<O, T, U> {
    data: O,
    init_value: U,
    reduce_function: fn(U, Tuple<T>) -> U,
}

impl<O, T, U> Operation<U> for ReduceOperation<O, T, U>
where
    O: Operation<T>,
    T: Copy,
    U: Copy,
{
    fn eval(&self) -> ConsData<U> {
        let fold_result = self
            .data
            .eval()
            .into_iter()
            .fold(self.init_value, self.reduce_function);
        vec![Tuple::new(fold_result)]
    }
}

// REDUCE BY KEY
struct ReduceByKeyOperation<O, T, U, K, O2> {
    data: O,
    transform_function: fn(Tuple<T>) -> (K, O2),
    init_value: U,
    reduce_function: fn(U, O2) -> U,
}

impl<O, T, U, K, O2> Operation<(K, U)> for ReduceByKeyOperation<O, T, U, K, O2>
where
    O: Operation<T>,
    K: Eq + Hash,
    T: Copy + Hash + Eq,
    U: Copy,
{
    fn eval(&self) -> ConsData<(K, U)> {
        // Genero un HashMap con la clave y el valor
        // let mut result_by_key: HashMap<T, Vec<u32>> = HashMap::new();
        let mut result_by_key = HashMap::new();
        let data_eval = self.data.eval();
        let lenght_data = data_eval.len();
        for tuple in data_eval.into_iter() {
            let pair_tuple = Rdd::tuple_cons_to_pair_data(tuple, self.transform_function);
            let key = (pair_tuple.0).0;
            let values = (pair_tuple.1).0;
            let values_for_tuple_key = result_by_key.entry(key).or_insert(vec![]);
            values_for_tuple_key.push(values);
        }

        let mut result: ConsData<(K, U)> = Vec::with_capacity(lenght_data);
        for (key, values) in result_by_key {
            let reduced_values = values.into_iter().fold(self.init_value, self.reduce_function);
            result.push(Tuple::new((key, reduced_values)));
            // vec![Tuple::new(fold_result)]
        }
        result
    }
}

fn main() {
    // +++++++++ MAP +++++++++
    let a_map_funcion: fn(Tuple<usize>) -> Tuple<(usize, usize, bool)> =
        |tuple| Tuple::new((tuple.0, 4, true));
    let vec_of_tuples = vec![Tuple::new(1), Tuple::new(2), Tuple::new(3)];
    let operation = MapOperation {
        data: vec_of_tuples.clone(),
        map_function: a_map_funcion,
    };
    let result = operation.eval();
    println!("Result -> {:?}", result); // Expected [(1,3),(1,4),(1,5)]

    // Otro Map
    let a_map_funcion_2: fn(Tuple<(usize, usize, bool)>) -> Tuple<(bool)> =
        |tuple| Tuple::new((tuple.0).0 % 2 == 0);
    let operation_2 = MapOperation {
        data: result,
        map_function: a_map_funcion_2,
    };
    let result = operation_2.eval();
    println!("Result -> {:?}", result); // Expected [false, true, false]

    // +++++++++ FILTER +++++++++
    let a_filter_function: fn(&Tuple<(bool)>) -> bool = |tuple| (*tuple).0;
    let operation_3 = FilterOperation {
        data: result,
        filter_function: a_filter_function,
    };
    let result = operation_3.eval();
    println!("Result -> {:?}", result); // Expected [true]

    // +++++++++ REDUCE +++++++++
    let a_reduce_function: fn(usize, Tuple<usize>) -> usize = |acc, tuple| acc + tuple.0;
    let operation_4 = ReduceOperation {
        data: vec_of_tuples.clone(),
        init_value: 0,
        reduce_function: a_reduce_function,
    };
    let result = operation_4.eval();
    println!("Result -> {:?}", result); // Expected [6]
    
    // +++++++++ ANIDADO! +++++++++
    // Hago el mismo funcionamiento que se hizo arriba
    let nested_operation = FilterOperation {
        data: MapOperation {
            data: operation,
            map_function: a_map_funcion_2,
        },
        filter_function: a_filter_function,
    };
    let result = nested_operation.eval();
    println!("Nested result -> {:?}", result); // Expected [true]

    // REDUCE CON CAMBIO DE TIPOS
    let a_reduce_function: fn(bool, Tuple<usize>) -> bool = |acc, tuple| acc && (tuple.0 > 1);
    let operation_5 = ReduceOperation {
        data: vec_of_tuples.clone(),
        init_value: false,
        reduce_function: a_reduce_function,
    };
    let result = operation_5.eval();
    println!("Result reduce cambio de tipo -> {:?}", result); // Expected [false]

    // Ejemplo de WordCount!
    let words = vec!["Hola", "mundo", "chau", "mundo"];
    let words = Rdd::new_data_from_vec(words);
    let add_1_function: fn(Tuple<&str>) -> Tuple<(&str, usize)> = |tuple| Tuple::new((tuple.0, 1));
    let map = MapOperation {data: words, map_function: add_1_function};
    let resultado_map_1 = map.eval();
    println!("Result -> {:?}", resultado_map_1); // Expected [("Hola", 1), ("mundo", 1), ("chau", 1), ("mundo", 1)]

    // Especifico una funcion para definir claves valores
    let set_key: fn(Tuple<(&str, usize)>) -> (&str, usize) = |tuple| {
        let values = tuple.0;
        (values.0, values.1)
    };

    let reduce_function: fn(usize, usize) -> usize = |acc, tuple| acc + tuple;

    let reduce_by_key_op = ReduceByKeyOperation {
        data: resultado_map_1,
        transform_function: set_key,
        init_value: 0,
        reduce_function: reduce_function,
    };

    let result_wordcount = reduce_by_key_op.eval();
    println!("WordCount! -> {:?}", result_wordcount);
}
