// Hay dos tipos de errores: los recuperables y los no recuperables
// Para los primeros se usa Result
// Para los segundos se usa la macro panic!

// Aca se tratan los segundos (panic!)
// Como dice en https://doc.rust-lang.org/stable/book/ch09-01-unrecoverable-errors-with-panic.html#unwinding-the-stack-or-aborting-in-response-to-a-panic
// Rust hace una limpieza profunda de la memoria. Esta limpieza podría delegarse al SO
//  para que el binario compilado resulte más pequeño (no se cuanto).

// Seteando la variable de entorno RUST_BACKTRACE=1 o RUST_BACKTRACE=full
// se puede obtener un backtrace completo de la ejecucion

fn main() {
    // Llamado explicito
    // panic!("Error irrecuperable");

    // Error ocurrido en otras librerias por un bug
    let v = vec![1, 2, 3];

    v[99];
}