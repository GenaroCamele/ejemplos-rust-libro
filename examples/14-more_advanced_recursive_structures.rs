use std::rc::Rc;

enum List<T, H, F = fn(T) -> H>
{
    Nil,
    Cons(T, Rc<List<T, H>>),
    Map(F, Rc<List<T, H>>)
}

// Especifico F como una funcion que toma T
// Y a H como una tupla vacia () ó tambien llamada Unit
// Ambos parametros dummy para que no joda con el tamaño de los
// parametros en tiempo de ejecucion
impl<T, H> List<T, H, fn(T) -> H> {
    fn new_data(data: T) -> Self {
        List::Cons(
            data,
            Rc::new(List::Nil)
        )
    }
    fn add_map(a_function: fn(T) -> H, a_list: List<T, H, fn(T) -> H>) -> Self {
        List::Map(a_function, Rc::new(a_list))
    }
}

// Cuenta la cantidad de elementos que hay en la lista
fn count_layers_in_structure<T, F, H>(list: &List<T, F, H>) -> u32 {
    match list {
        List::Nil => 0,
        List::Cons(_, e1) => 1 + count_layers_in_structure(&(*e1)),
        List::Map(_, e1) => 1 + count_layers_in_structure(&(*e1)),
    }
}

// fn eval<T, F, H>(list: &List<T, F, H>) -> List<T, F, H> {
//     match list {
//         List::Nil => List::Nil,
//         List::Cons(_, e1) => 1 + eval(&(*e1)),
//         List::Map(_, e1) => 1 + eval(&(*e1)),
//     }
// }

fn main() {
    let list = List::new_data(32);

    let map_function = |x: u32| x + 2;
    let list2 = List::add_map(map_function, list);

    let count_layers = count_layers_in_structure(&list2);
    println!("La cantidad de elementos en la lista es -> {}", count_layers);

    // let count_layers = eval(&list2);
    // println!("La cantidad de elementos en la lista es -> {}", count_layers);
}