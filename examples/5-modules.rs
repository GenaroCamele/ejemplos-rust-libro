// Defino modulos dentro del mismo archivo
// Por defecto son PRIVADOS
mod hpc {
    pub struct Master {
        pub ip: String, // Dentro de los modulos, todos los campos de los structs son privados a menos que se indique lo contrario
        port: String, // Este es privado
    }

    impl Master {
        pub fn create_from(ip: String, port: String) -> Self {
            Master {
                ip,
                port
            }
        }

        // Aca puedo acceder a port porque es una impl de la struct dentro del modulo
        pub fn print_info(&self) {
            println!("IP = {} | Port = {}", self.ip, self.port);
        }
    }
}

// Para acortar la cantidad de caminos para la utilizacion de algun componente del modulo
use hpc::Master;

mod master_and_worker;
use master_and_worker::{MasterMod, WorkerMod};

fn main() {
    // Se puede arrancar desde "hpc::..." porque esta en el mismo archivo,
    let master = hpc::Master::create_from(String::from("127.0.0.1"), String::from("8000"));
    
    // tambien se podria importar con la ruta absoluta empezando por "crate::hpc::..."
    // let _master2 = crate::hpc::Master::create_from(String::from("127.0.0.1"), String::from("8000"));
    
    // Otra forma es utilizar la sentencia "use" para acortar las rutas
    let _master3 = Master::create_from(String::from("127.0.0.1"), String::from("8000"));
    
    // Llamamos a los metodos publicos
    master.print_info();
    println!("{}", master.ip); // No hay problema, el campo ip es PUBLICO
    // println!("{}", master.port) // HAY problema, el campo port es PRIVADO

    // Acceso a los modulos de otro archivo
    let master4 = MasterMod::create_from(String::from("127.0.0.1"), String::from("8000"));
    master4.print_info();

    let worker = WorkerMod::create_worker_from(String::from("127.0.0.1"), String::from("8000"));
    worker.print_info();
}