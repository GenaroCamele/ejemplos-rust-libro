use std::sync::mpsc;
use std::thread;
use std::time::Duration;

fn main() {
    // Creamos un canal. "MPSC" es por "Multiple Produce Simple Consumer"
    // Es decir, que se envian multiples mensajes y solo habra un thread recibiendolo
    // tx => Transmitter
    // rx => Receiver
    let (tx, rx) = mpsc::channel();

    // Creamos un thread que envia informacion
    thread::spawn(move || {
        let val = String::from("hi");
        // Espero dos segundos para demostrar que el receive es BLOQUEANTE
        thread::sleep(Duration::from_millis(2000));
        
        // Si no existe un rx o fue dado de baja (por ejemplo, porque salio del scope y fue liberado)
        // el send arrojara error. En este caso hara panic, pero se podria manejar mejor
        tx.send(val).unwrap();

        // NOTA: el send tambien toma el ownership
        // println!("val -> {}", val); // ERROR: val ya fue enviado
    });

    // Recibo en el thread principal el valor enviado de manera BLOQUEANTE
    // Para una solucion NO BLOQUEANTE utilizar "try_recv" que devuelve un
    // Result con el mensaje en el Ok en caso de que haya uno en ese momento, y
    // Err en caso de que no haya ninguno
    let received = rx.recv().unwrap();
    println!("Got: {}", received);


    // Esto arroja error porque no hay nada mas para recibir
    match rx.recv() {
        Ok(msg) => println!("Mensaje recibido -> {}", msg),
        Err(error) => println!("No hay mas mensajes para obtener. Error lanzado -> \"{}\"", error)
    };

    // Podemos hacer multiples send y recv con iteradores!!!
    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        let v1 = vec![
            String::from("Hola"),
            String::from("Mundo"),
            String::from("Chau"),
        ];

        for elem in v1 {
            tx.send(elem).unwrap();
        }
    });

    // Aca se llama al .recv IMPLICITAMENTE!
    // El iterador se cierra de forma automatica cuando no hay mas mensaje para recibir
    for msg in rx {
        println!("Mensaje recibido en loop! -> {}", msg);
    }

    // MULTIPLES PRODUCTORES
    let (tx, rx) = mpsc::channel();

    // Podemos clonar el Transmitter
    let tx_1 = mpsc::Sender::clone(&tx);

    thread::spawn(move || {
        tx.send(String::from("Soy el Thread 1")).unwrap();
    });

    thread::spawn(move || {
        tx_1.send(String::from("Soy el Thread 2")).unwrap();
    });

    for msg in rx {
        println!("Mensaje de mutiples productores -> {}", msg);
    }
}