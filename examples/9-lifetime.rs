
// Retorna el elemento con la longitud mas larga
// Al igual que hacemos con los tipos, podemos especificar
// lifetimes genericos para indicar que tanto los argumentos como
// el tipo de retorno va a tener el mismo tiempo de vida y no va a
// retornar referencias a memoria invalida
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

// Se puede tambien aplicar a structs e impls
struct ImportantExcerpt<'a> {
    hola: &'a str
}

// En el caso de un impl que sea para un Struct con lifetime generico
// SE DEBE especificar el lifetime despues del nombre del struct como
// se hace debajo
impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        3
    }
}

fn main() {
    // Ejemplo 1
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);

    // Ejemplo 2
    // Notese que string1 y string2 tienen diferentes lifetimes.
    // Al ponerlo generico, Rust se quedara con el menor tiempo de vida,
    // en este caso, el lifetime de string2
    let string1 = String::from("long string is long");

    {
        let string2 = String::from("xyz");
        let result = longest(string1.as_str(), string2.as_str());
        // result solo sera valida el mismo tiempo de vida que string2
        // como lo indica la logica del lifetime generico
        println!("The longest string is {}", result);
    }
}