// mod master_and_worker;
// use master_and_worker::WorkerMod;

extern crate ahash;

use std::collections::HashMap;
use ahash::ABuildHasher;

fn print_key(ages: &HashMap<String, u32>, key_to_recover: &String) {
    match ages.get(key_to_recover) {
        Some(value) => println!("La edad de {} es {}", key_to_recover, value),
        None => println!("No existe la clave {} en el HashMap", key_to_recover)
    }
}

fn print_key_hasher(ages: &HashMap<String, u32, ahash::ABuildHasher>, key_to_recover: &String) {
    match ages.get(key_to_recover) {
        Some(value) => println!("La edad de {} es {}", key_to_recover, value),
        None => println!("No existe la clave {} en el HashMap", key_to_recover)
    }
}

fn main() {
    // VECTORES

    // Cuando no le asignamos valores en la creacion se debe especificar el tipo
    let _v1: Vec<u8> = Vec::new();

    // Puede ser de cualquier tipo!
    // let _v2: Vec<WorkerMod> = Vec::new();

    // Si queremos que se infiera el tipo debemos brindar valores iniciales
    // y hacer uso de la macro "vec!"
    let _v3 = vec![1, 2, 3];
    
    // Para agregar elementos debe ser mutable
    // En este caso como se agregar elementos se puede inferir el tipo,
    // El "Vec<i32>" es opcional
    let mut v4: Vec<i32> = Vec::new();
    v4.push(2);
    v4.push(3);
    v4.push(37);
    v4.push(23);
    v4.push(53);
    v4.push(33);
    
    // Eliminar elementos
    v4.pop();

    // Acceder a los elementos
    // NOTA: en la documentacion lo manejan con & el acceso, no se por que,
    // porque no se rompe nada...
    // let third_elem: &i32 = &v4[2];

    let third_elem = v4[2];
    println!("El tercer elemento es {}", third_elem);
    // println!("{}", v4[2]); // No se por que no se rompe...

    // Se puede hacer con Match para saber si hay algo o no
    // Devuelve un opcional
    match v4.get(55) {
        Some(elem) => println!("El elemento es {}", elem),
        None => println!("El elemento no existe")
    }

    // Iterando sobre el arreglo
    for elem in &v4 {
        println!("Elemento actual -> {}", elem);
    }

    // Las iteraciones sobre el arreglo pueden ser mutables!!
    for elem in &mut v4 {
        *elem += 2; // NOTESE el * para acceder al valor de la referencia
    }

    // NOTA: para Strings no se pueden recorrer por letras o acceder a un caracter directamente
    // vale la pena leer el capitulo dedicado a esta estructura: https://doc.rust-lang.org/stable/book/ch08-02-strings.html

    // HASH MAPS
    let mut ages = HashMap::new();
    let name1 = String::from("Genaro");
    ages.insert(name1, 24);
    ages.insert(String::from("Chiara"), 22);

    // println!("{}", name1); // ERROR! Los HashMaps toman la pertenencia en los inserts

    // Accediendo a los elementos
    let key_to_recover = String::from("Genaro");
    let key_invalid = String::from("ASSSS");
    
    print_key(&ages, &key_to_recover);
    print_key(&ages, &key_invalid);

    // Puedo recorrer todas las claves y los valores
    for (key, value) in &ages {
        println!("{}: {}", key, value);
    }

    // Podemos reemplazar valores antiguos
    ages.insert(String::from("Genaro"), 62);

    // Podemos solo insertar si no existe
    let name_camila = String::from("Camila");
    ages.entry(name_camila.clone()).or_insert(12);
    ages.entry(name_camila.clone()).or_insert(23);

    print_key(&ages, &name_camila); // Imprime 12 porque el 23 no lo inserto porque la clave ya existia
    
    // Podemos reemplazar el valor utilizando el valor viejo de la clave
    let entry = ages.entry(name_camila.clone()).or_insert(0); // El or_insert devuelve la referencia al valor de la clave
    *entry += 1; // Se cambia el valor al que apunta esa referencia
    print_key(&ages, &name_camila); // Imprime 13

    // IMPORTANTE: se puede cambiar el hasher de los HashMaps para que sea mas rapidos (a un costo de menor seguridad)
    let mut hash_map_faster = HashMap::with_hasher(ABuildHasher::new());
    let name_genarete = String::from("Genarete");
    hash_map_faster.insert(name_genarete.clone(), 5);
    print_key_hasher(&hash_map_faster, &name_genarete);

    // Algunos de los ejercicios que proponen en https://doc.rust-lang.org/stable/book/ch08-03-hash-maps.html#hashing-functions
    // Promedio y los numeros que mas aparecen en un vector
    let v_numbers = vec![1, 2, 3, 4, 5, 6, 7, 7, 8];
    let mut mean = 0;
    let mut occurrences = HashMap::with_hasher(ABuildHasher::new()); // Me hago un HashMap rapido
    for elem in &v_numbers {
        mean += elem;
        let number_count = occurrences.entry(*elem).or_insert(0); // Tomo la entrada, si no existia la inicializo en 0
        *number_count += 1; // Le sumo uno
    }
    mean /= v_numbers.len();
    println!("El promedio es {}", mean);
    println!("{:?}", occurrences);

    // Tuplas con elementos
    let a = (1, 2, true, 4);
    println!("Tupla con elementos {:?}", a);
    println!("Tupla primer elemento -> {:?}", a.0);

}