use std::sync::{Mutex, Arc};
use std::thread;

fn main() {
    // Defino un semaforo para la exclusion mutua
    let m = Mutex::new(8);

    // Se pone adentro de un bloque para que cuando
    // num salga del scope libere el lock AUTOMATICAMENTE!! <emoji de pera>
    {
        let mut num = m.lock().unwrap();
        *num = 6;
    }
    println!("m = {:?}", m);

    // PROBLEMA con exclusion mutua
    let counter = Mutex::new(0);
    let mut handles = vec![];

    for _ in 0..10 {
        let handle = thread::spawn(move || {
            // ERROR: no se puede acceder en la segunda iteracion porque en la primera
            // ya se le concedio la pertenencia del counter al primer thread
            // let mut num = counter.lock().unwrap();

            // *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());

    // Para SOLUCIONAR EL PROBLEMA, hay que poner al Mutex dentro de Smart Pointer de multiples
    // ownership como Rc, pero que pueda realizar el conteo de referencias de manera atomica. Para
    // ellos usamos Arc
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter); // Genero un puntero al Mutex
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();

            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}
