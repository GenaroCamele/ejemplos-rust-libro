
#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

impl<T> ReturnedType for Tuple<T> {}

trait ReturnedType {}

fn recursivo<T>(tuple: Tuple<T>, change: bool) -> Box<dyn ReturnedType>
where T: 'static
{
    if change {
        Box::new(tuple)
    } else {
        // Hace recursion
        // let rec_res_box = recursivo(tuple, true);
        // let rec_res = *rec_res_box;
        // Tuple::new((1, 2, 3))
        // Box::new((rec_res.0, String::from("hola")))
        Box::new(Tuple::new((1, 2, 3)))
    }
}


fn main() {
    let tuple = Tuple::new(('1', '2', 'a', true));

    let _res = recursivo(tuple, false);
    // println!("Resultado -> {:?}", res);
}