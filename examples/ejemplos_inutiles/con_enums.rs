use std::process;

mod common_tp;
use common_tp::{get_flattened, read_file};

use std::rc::Rc;

#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Tipo de la funciones Map
type MapFunction<T, H> = fn(Tuple<T>) -> Tuple<H>;

enum Rdd<T, H, F = MapFunction<T, H>> {
    Cons(Vec<Tuple<T>>),
    Map(F, Rc<Rdd<T, H>>),
}

#[derive(Debug)]
enum EvalResult<C, M> {
    ConsResult(Vec<Tuple<C>>),
    MapResult(Vec<Tuple<M>>),
}

impl<T, H> Rdd<T, H, MapFunction<T, H>> {
    // Arma un RDD con tantas tuplas como valores tenga el arreglo
    fn new_data_from_vec(data_vector: Vec<T>) -> Self {
        let mut final_data: Vec<Tuple<T>> = Vec::new();

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        Rdd::Cons(final_data)
    }

    fn add_map(a_function: MapFunction<T, H>, rdd: Rdd<T, H, MapFunction<T, H>>) -> Self {
        Rdd::Map(a_function, Rc::new(rdd))
    }
}

fn eval<T, H>(rdd: &Rdd<T, H, MapFunction<T, H>>) -> EvalResult<T, H>
where
    T: Clone + Send + Sync + Eq + std::hash::Hash + std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => EvalResult::ConsResult(data.to_vec()),
        Rdd::Map(map_function, e1) => {
            let recursive_result = eval(&e1);
            match recursive_result {
                EvalResult::ConsResult(data_vector) => {
                    let map_result = data_vector
                        .into_iter()
                        .map(map_function)
                        .collect::<Vec<Tuple<_>>>();

                    EvalResult::MapResult(map_result)
                }
                EvalResult::MapResult(data_vector) => {
                    let map_result = data_vector
                        .into_iter()
                        .map(map_function)
                        .collect::<Vec<Tuple<_>>>();

                    EvalResult::MapResult(map_result)
                },
            }
        }
    }
}

fn main() {
    let content = read_file();
    if let Err(error) = content {
        println!("Error al leer el archivo -> {:#?}", error);
        process::exit(1);
    }

    // Obtengo y hago un flatten del contenido
    let content = content.unwrap();
    let content_flattened = get_flattened(content);
    // println!("Contenido del archivo {:#?}", content_flattened); // Imprimo el contenido del archivo

    // Creo un RDD con los valores flattened del archivo leido
    let rdd = Rdd::new_data_from_vec(content_flattened);

    // Agrego una funcion que agrega un 1 a todas las palabras
    let map_function = |tuple: Tuple<(String)>| Tuple::new((tuple.0, 1));
    let rdd_2 = Rdd::add_map(map_function, rdd);
    test_rdd(&rdd_2); // DEBUG: Imprime lo que hay en el RDD.
    // Evaluo el grafico de ejecucion
    let result_eval = eval(&rdd_2);
    println!(
        "Resultado final de evaluación RDD_2 (un solo MAP) -> {:?}",
        result_eval
    );

    if let EvalResult::MapResult(res) = result_eval {
        println!("Es un resultado! -> {:?}", res);
        println!("Es un resultado! -> {:?}", res[0].0);
    }

    let map_function_2 = |tuple: Tuple<_>| Tuple::new((tuple.0, 2));
    let rdd_3 = Rdd::add_map(map_function_2, rdd_2);
    let result_eval = eval(&rdd_3);
    println!(
        "Resultado final de evaluación RDD_3 (dos MAP) -> {:?}",
        result_eval
    );
}

// Imprime los datos de un RDD que es solo Cons. Sirve solo para DEBUG
fn test_rdd<T, H>(rdd: &Rdd<T, H, MapFunction<T, H>>)
where
    T: std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => println!("Data del RDD -> {:?}", data),
        Rdd::Map(_, e1) => {
            println!("\tMap");
            test_rdd(&e1)
        }
    }
}
