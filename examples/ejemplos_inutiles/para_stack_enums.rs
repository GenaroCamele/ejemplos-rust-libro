// Tipo de funcion que puede recibir Operacion::Cambiar
type FuncionCambiar<T, U> = fn(T) -> U;

// Estructura recursiva
enum Operacion<T, U, F = FuncionCambiar<T, U>>
where
    F: Fn(T) -> U,
{
    Mantener(Vec<T>),
    Cambiar(F, Box<Operacion<T, U>>),
}

// En cada recursion si hay operaciones "Operacion::Cambiar", el tipo de
// Vec a devolver cambia!
fn eval<T, U>(operacion: &Operacion<T, U, FuncionCambiar<T, U>>) -> Vec<T>
where
    std::vec::Vec<T>: std::iter::FromIterator<U>,
{
    match operacion {
        Operacion::Mantener(vector) => *vector,
        Operacion::Cambiar(una_funcion, recursion) => eval(&recursion)
            .into_iter()
            .map(una_funcion)
            .collect::<Vec<T>>(),
    }
}

fn main() {
    // Defino una funcion que va a cambiar mis datos
    let funcion_map = |x| (1, x + 2);

    // Defino el enum recursivo con la funcion
    let operacion = Operacion::Cambiar(
        funcion_map,
        Box::new(Operacion::Mantener(vec![1, 2, 3])),
    );

    let resultado = eval(&operacion);
    println!("Resultado -> {:?}", resultado); // Esperaria [(1,3),(1,4),(1,5)]
}
