use std::process;
mod common_tp;
use common_tp::{get_flattened, read_file};
// use std::rc::Rc;

#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Trait para que sea todo generico
trait Evaluate {
    type ItemToReceive;
    type ItemToReturn;
    fn compute(&self, data: Vec<Tuple<Self::ItemToReceive>>) -> Vec<Tuple<Self::ItemToReturn>>;
}

// Estructura de datos constante
struct Data<T> {
    data: Vec<Tuple<T>>,
}

impl<T> Data<T> {
    // Arma un RDD con tantas tuplas como valores tenga el arreglo
    fn new_data_from_vec(data_vector: Vec<T>) -> Self {
        let mut data: Vec<Tuple<T>> = Vec::new();

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        Data { data }
    }
}

impl<T> Evaluate for Data<T>
where
    T: Clone,
{
    type ItemToReceive = T;
    type ItemToReturn = T;
    fn compute(&self, _: Vec<Tuple<T>>) -> Vec<Tuple<T>> {
        self.data.to_vec()
    }
}

// Estructura con la funcion Map
type MapFunction<T, U> = fn(Tuple<T>) -> Tuple<U>;

struct Map<T, U> {
    function: MapFunction<T, U>,
}

impl<T, U> Map<T, U> {
    fn new(function: MapFunction<T, U>) -> Self {
        Map { function }
    }
}

impl<T, U> Evaluate for Map<T, U>
where
    T: Clone,
{
    type ItemToReceive = T;
    type ItemToReturn = U;
    fn compute(&self, data: Vec<Tuple<T>>) -> Vec<Tuple<U>> {
        data.into_iter()
            .map(self.function)
            .collect::<Vec<Tuple<U>>>()
    }
}

// Grafo con los nodos a ejecutar
struct Graph<T, U> {
    nodes: Vec<Box<dyn Evaluate<ItemToReceive = T, ItemToReturn = U>>>,
}

impl<T, U> Graph<T, U>
where
    T: std::fmt::Debug,
    U: std::fmt::Debug,
{
    fn new() -> Self {
        Graph { nodes: vec![] }
    }

    fn eval(&self)/*  -> Vec<Tuple<U>> */ {
        let mut result = vec![];
        for node in self.nodes.iter() {
            result = node.compute(result);
        }
        println!("Resultado del eval -> {:?}", result);
    }

    fn add_node(&mut self, node: Box<dyn Evaluate<ItemToReceive = T, ItemToReturn = U>>) {
        self.nodes.push(node);
    }
}

fn main() {
    let content = read_file();
    if let Err(error) = content {
        println!("Error al leer el archivo -> {:#?}", error);
        process::exit(1);
    }

    // Obtengo y hago un flatten del contenido
    let content = content.unwrap();
    let content_flattened = get_flattened(content);

    let mut graph = Graph::new();
    graph.add_node(Box::new(Data::new_data_from_vec(content_flattened)));
    graph.eval();
    // println!("Resultado del eval -> {:?}", eval_result);

    let a_map_function = |tuple| tuple;
    graph.add_node(Box::new(Map::new(a_map_function)));
    graph.eval();
    // println!("Resultado del eval -> {:?}", eval_result);
}
