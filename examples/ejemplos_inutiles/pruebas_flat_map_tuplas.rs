use std::fs::File;
use std::io::{self, Read};
use std::process;
use std::rc::Rc;
use std::collections::HashMap;
extern crate rayon;
use rayon::prelude::*;

#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Tipo de la funciones Map
type MapFunction<T> = fn(Tuple<T>) -> Tuple<T>;
// type MapFunction<T> = fn(&Tuple<T>) -> Tuple<T>;

enum Rdd<T, F = MapFunction<T>> {
    Cons(Vec<Tuple<T>>),
    Map(F, Rc<Rdd<T>>),
    ReduceByKey(F, Rc<Rdd<T>>)
}

impl<T> Rdd<T, MapFunction<T>> {
    // Arma un RDD con tantas tuplas como valores tenga el arreglo
    fn new_data_from_vec(data_vector: Vec<T>) -> Self {
        let mut final_data: Vec<Tuple<T>> = Vec::new();

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        Rdd::Cons(final_data)
    }

    fn add_map(a_function: MapFunction<T>, rdd: Rdd<T, MapFunction<T>>) -> Self {
        Rdd::Map(a_function, Rc::new(rdd))
    }

    fn add_reduce_by_key(a_function: MapFunction<T>, rdd: Rdd<T, MapFunction<T>>) -> Self {
        Rdd::ReduceByKey(a_function, Rc::new(rdd))
    }
}

// Lee el contenido de un archivo y lo devuelve
fn read_file() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("/home/genaro/Proyectos/pruebasRust/examples/test.txt")?.read_to_string(&mut s)?;
    Ok(s) // Si llega aca entonces la linea de arriba no hizo el return de error
}

// Recibe el contenido de un archivo y lo devuelve flattened
fn get_flattened(content: String) -> Vec<(String, usize)> {
    let content = content.lines().collect::<Vec<&str>>();
    let flattened_ref = content
        .iter()
        .flat_map(|line| line.split_whitespace())
        .collect::<Vec<&str>>();
    
    // TODO: se concateno el 1 para que no falle por tipos la forma de la tupla (String, usize)
    // TODO: pero esta re contra re harcodeado, no deberia ser asi
    flattened_ref.par_iter().map(|word| (word.to_string(), 1)).collect()
}

fn eval<T>(rdd: &Rdd<T, MapFunction<T>>) -> Vec<Tuple<T>>
    where T: Clone + Send + Sync + Eq + std::hash::Hash + std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => data.to_vec(),
        Rdd::Map(map_function, e1) => {
            // eval(&e1).into_iter().map(map_function).collect::<Vec<Tuple<_>>>()
            // TODO: Aca esta paralelizado
            eval(&e1).into_par_iter().map(map_function).collect::<Vec<Tuple<_>>>()
        },
        Rdd::ReduceByKey(_reduce_function, e1) => {
            let result = eval(&e1);
            // TODO: no funciona... Para variar...
            // let mut occurrences = HashMap::new();
            // for elem in result.iter() {
            //     let number_count = occurrences.entry(&elem.0).or_insert(0); // Tomo la entrada, si no existia la inicializo en 0
            //     *number_count += 1; // Le sumo uno
            // }

            // // println!("HashMap {:#?}", occurrences);

            // let mut vec_result = Vec::new();
            // for (word, number_count) in &occurrences {
            //     // println!("{:?}: {:?}", word, number_count);
            //     let new_tuple = Tuple::new((word, *number_count));
            //     println!("Tupla -> {:?}", new_tuple);
            //     vec_result.push(new_tuple)
            // }
            // vec_result
            result
        }
    }
}

// Imprime los datos de un RDD que es solo Cons. Sirve solo para DEBUG
fn test_rdd<T>(rdd: &Rdd<T, MapFunction<T>>)
where
    T: std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => println!("Data del RDD -> {:#?}", data),
        Rdd::Map(_, e1) => {
            // TODO: esto funciona, hay que ver como carajo iterar sobre esto
            println!("\tMap");
            test_rdd(&e1)
        }
        Rdd::ReduceByKey(_, e1) => {
            // TODO: esto funciona, hay que ver como carajo iterar sobre esto
            println!("\tReduceByJey");
            test_rdd(&e1)
        }
    }
}

fn main() {
    let content = read_file();
    if let Err(error) = content {
        println!("Error al leer el archivo -> {:#?}", error);
        process::exit(1);
    }

    // Obtengo y hago un flatten del contenido
    let content = content.unwrap();
    let content_flattened = get_flattened(content);
    // println!("Contenido del archivo {:#?}", content_flattened); // Imprimo el contenido

    let rdd = Rdd::new_data_from_vec(content_flattened);
    // test_rdd(&rdd); // DEBUG: Imprime lo que hay en el RDD. Deberia haber tantas tuplas como palabras en el archivo
    // let result = eval(&rdd);

    // let map_function = |tuple: Tuple<_>| tuple; // Funciona
    // let map_function = |tuple: Tuple<_>| Tuple::new(format!("{}asssss", tuple.0));
    let map_function = |tuple: Tuple<_>| tuple;
    let rdd_2 = Rdd::add_map(map_function, rdd);
    // test_rdd(&rdd_2); // DEBUG: Imprime lo que hay en el RDD.

    let reduce_function = |tuple: Tuple<_>| tuple;
    let rdd_3 = Rdd::add_reduce_by_key(reduce_function, rdd_2);
    
    let result = eval(&rdd_3);
    // for tuple in result.iter() {
    //     println!("Tupla resultante -> {:?}", tuple);
    // }
    
    let mut occurrences: HashMap<String, usize> = HashMap::new();
    for elem in result.iter() {
        let word: String = ((elem.0).0).to_string();
        let number_count = occurrences.entry(word).or_insert(0); // Tomo la entrada, si no existia la inicializo en 0
        *number_count += 1; // Le sumo uno
    }
    
    // println!("HashMap {:#?}", occurrences);
    
    // let mut vec_result = Vec::new();
    // for (word, number_count) in &occurrences {
        // println!("{:?}: {:?}", word, number_count);
        // let new_tuple = Tuple::new((word, *number_count));
        // println!("Tupla -> {:?}", new_tuple);
        // vec_result.push(new_tuple)
    // }
    println!("Vector de resultados -> {:?}", occurrences);
}
