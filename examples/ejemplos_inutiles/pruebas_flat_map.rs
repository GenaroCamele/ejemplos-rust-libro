use std::fs::File;
use std::io::{self, Read};
use std::process;
use std::rc::Rc;

enum Rdd<T,  H,  F = fn(Vec<T>) -> Vec<H>> {
    Cons(Vec<Vec<T>>),
    Map(F, Rc<Rdd<T, H>>),
}

impl<T, H> Rdd<T, H, fn(Vec<T>) -> Vec<H>> {
    fn new_data(data: Vec<T>) -> Self {
        let mut final_data: Vec<Vec<T>> = Vec::new();
        final_data.push(data);
        Rdd::Cons(final_data)
    }

    fn add_map(a_function: fn(Vec<T>) -> Vec<H>, execution_graph: Rdd<T, H, fn(Vec<T>) -> Vec<H>>) -> Self {
        Rdd::Map(a_function, Rc::new(execution_graph))
    }
}

// Lee el contenido de un archivo y lo devuelve
fn read_file() -> Result<String, io::Error> {
    let mut s = String::new();
    File::open("/home/genaro/Proyectos/pruebasRust/examples/test.txt")?.read_to_string(&mut s)?;
    Ok(s) // Si llega aca entonces la linea de arriba no hizo el return de error
}

// Recibe el contenido de un archivo y lo devuelve flattened
fn get_flattened(content: String) -> Vec<String> {
    let content = content.lines().collect::<Vec<&str>>();
    let flattened_ref = content.iter().flat_map(|line| line.split_whitespace()).collect::<Vec<&str>>();
    flattened_ref.iter().map(|word| word.to_string()).collect()
}

// Genera la evaluacion de un grafo de ejecucion
fn eval<T, H>(rdd: &Rdd<T, H, fn(Vec<T>) -> Vec<H>>) -> Vec<Vec<T>>
where
    T: std::clone::Clone,
    std::vec::Vec<std::vec::Vec<T>>: std::iter::FromIterator<std::vec::Vec<H>>,
{
    match rdd {
        Rdd::Cons(data) => data.to_vec(),
        Rdd::Map(closure, e1) => eval(&(*e1))
            .into_iter()
            .map(closure)
            .collect(),
    }
}

fn main() {
    let content = read_file();
    if let Err(error) = content {
        println!("Error al leer el archivo -> {:#?}", error);
        process::exit(1);
    }

    // Obtengo y hago un flatten del contenido
    let content = content.unwrap();
    let content_flattened = get_flattened(content);
    // println!("Contenido del archivo {:#?}", content_flattened); // Imprimo el contenido

    let rdd = Rdd::new_data(content_flattened);
    let map_function = |tuple: Vec<String>| tuple.push(1);
    let rdd_2 = Rdd::add_map(map_function, rdd);
}
