{-# LANGUAGE GADTs #-}

data Operacion a where
    Mantener :: [a] -> (Operacion a)
    Cambiar :: (a -> b) -> (Operacion a) -> (Operacion b)

eval :: Operacion a -> [a]
eval (Mantener vector) = vector
eval (Cambiar f recursion) = map f (eval recursion)