use std::fmt;

// FUNCIONA PERO NO SE PUEDE UTILIZAR MAP...

enum Rdd<T: 'static , G: 'static = fn(&T) -> (), H = ()>
where
    G: (Fn(&'static T) -> H),
{
    Data(Vec<T>),
    Map(G, Box<Rdd<T, G, H>>)
}

impl<T> Rdd<T, fn(&T), ()> { // for example
    fn new_data(data: Vec<T>) -> Self {
        Rdd::Data(data)
    }
}

impl<T: 'static, G: 'static, H> fmt::Debug for Rdd<T, G, H>
where G: (Fn(&'static T) -> H),
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
       match &*self {
           Rdd::Data(_) => write!(f, "Data"),
           Rdd::Map(_, e1) => write!(f, "Map {:?}", e1),
       }
    }
}

fn main() {
    let v1: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
    let rdd_1 = Rdd::new_data(v1);
    println!("Rdd -> {:?}", rdd_1);
    
    let map_fun = |x| x + 5;
    let v2: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
    let rdd_2 = Rdd::Map(map_fun, Box::new(
        Rdd::Data(v2)
    ));
    println!("Rdd -> {:?}", rdd_2);

    match rdd_2 {
        Rdd::Map(_, e1) => {
            match *e1 {
                Rdd::Data(data) => println!("{:?}", data),
                _ => println!("Errro")
            }
        },
        _ => println!("Errro")
    }
}