use std::fmt;

enum RunResult<T> {
    CountResult(usize),
    CollectResult(Vec<T>),
}

// Operaciones que diparan la ejecucion del grafo
enum RunOperation {
    Count,
    Collect,
    First, // TODO: Agregar el resto
}

// enum Rdd<T, G>
//     where G: (Fn(&T) -> T)
// {
//     Data(Vec<T>),
//     Map(G, Box<Rdd<T, G>>),
//     Cartesian(Box<Rdd<T, G>>, Box<Rdd<T, G>>),
//     Filter(G, Box<Rdd<T, G>>),
// }

enum Rdd<T: 'static, G: 'static = fn(&T) -> (), H = ()>
where
    G: (Fn(&'static T) -> H),
{
    Data(Vec<T>),
    Map(G, Box<Rdd<T, G, H>>),
}

impl<T> Rdd<T, fn(&T), ()> {
    // for example
    fn new_data(data: Vec<T>) -> Self {
        Rdd::Data(data)
    }
}

// Implementacion para poder debuguear el enum RDD
impl<G> fmt::Debug for Rdd<u32, G>
where
    G: (Fn(&u32) -> u32),
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            Rdd::Data(data) => write!(f, "Data {:?}", data),
            Rdd::Map(_, e1) => write!(f, "Map {:?}", e1),
            Rdd::Cartesian(e1, e2) => write!(f, "Cartesian ({:?}, {:?})", e1, e2),
            Rdd::Filter(_, e1) => write!(f, "Filter {:?}", e1),
        }
    }
}

// Ejecuta la accion de contar (accion disparadora COUNT) de manera recursiva
fn eval_count<T, G>(rdd: &Rdd<T, G>) -> usize
where
    G: (Fn(&T) -> T),
{
    match rdd {
        Rdd::Data(data) => data.len(),
        Rdd::Map(_, e1) => eval_count(&*e1),
        Rdd::Cartesian(e1, e2) => eval_count(&*e1) * eval_count(&*e2),
        Rdd::Filter(_a_filter_function, e1) => eval_count(&*e1), // TODO: Corregir una vez que se implemente la evaluacion total
    }
}

// Ejecuta la accion de ejecucion total (accion disparadora COLLECT) de manera recursiva
fn eval_complete<T, G>(rdd: &Rdd<T, G>) -> Vec<T>
where
    G: (Fn(&T) -> T),
{
    match rdd {
        Rdd::Data(data) => *data,
        Rdd::Map(a_map_function, e1) => eval_complete(&*e1).iter().map(a_map_function).collect(),
        // Rdd::Cartesian(e1, e2) => eval_complete(&*e1) * eval_complete(&*e2),
        // Rdd::Filter(a_filter_function, e1) => eval_complete(&*e1), // TODO: Corregir una vez que se implemente la evaluacion total
    }
}

// Funcion de evaluacion de un RDD. Decide si hay que ejecutar un count, un first,
// o una evaluacion completa
fn eval<T, G>(operation: RunOperation, rdd: &Rdd<T, G>) -> Option<RunResult<T>>
where
    G: (Fn(&T) -> T),
{
    match operation {
        RunOperation::Count => Some(RunResult::CountResult(eval_count(rdd))),
        RunOperation::Collect => Some(RunResult::CollectResult(eval_complete(rdd))),
        _ => {
            println!("Metodo no considerado");
            None
        }
    }
}

// Imprime el resultado
// TODO: borrar u32 del primer tipo del RDD, es solo para debug
fn print_result<'b, T, G>(rdd: &Rdd<u32, G>, res: Option<RunResult<T>>)
// where G: Fn(&'b T) -> T
where
    for<'r> G: (Fn(&'r u32) -> u32),
{
    let res_only_data = res.unwrap_or_else(|| panic!("Ocurrió un error al ejecutar"));

    match res_only_data {
        RunResult::CountResult(count) => {
            println!("Resultado de COUNT con el RDD {:?} -> {:#?}", rdd, count)
        }
        _ => println!("Todavia no esta implementado"),
    };
}

fn example_count() {
    let v1: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
    let rdd_1 = Rdd::Data(v1);
    let res = eval(RunOperation::Count, &rdd_1);

    print_result(&rdd_1, res);

    // Si le agregamos un map y no deberia cambiar la longitud
    let map_function = |x: u32| x + 1;
    let rdd_1_box: Box<Rdd<u32, _, _>> = Box::new(rdd_1);
    let rdd_2 = Rdd::Map(map_function, rdd_1_box);
    print_result(&rdd_2, eval(RunOperation::Count, &rdd_2));
    // Agregamos cartesianos, las longitudes deberian multiplicarse
    let v2: Vec<u32> = vec![1, 2, 3];
    let rdd_cart_1 = Rdd::Data(v2);

    let rdd_cart_1_box: Box<Rdd<u32, _>> = Box::new(rdd_cart_1);
    let rdd_cart_2_box: Box<Rdd<u32, _>> = Box::new(rdd_2);
    let rdd_3 = Rdd::Cartesian(rdd_cart_1_box, rdd_cart_2_box);
    print_result(&rdd_3, eval(RunOperation::Count, &rdd_3));

    // Si filtramos se deberia ejecutar todo
    // TODO: Completar despues de implementar la evaluacion total
}

// Ejecuta los ejemplos de evaluacion completa de un RDD
fn example_evaluate() {
    let v1: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
    let rdd_1 = Rdd::Data(v1);

    let v2: Vec<u32> = vec![1, 2, 3, 4, 5, 6];
    // let rdd_2 = Rdd::Data(v2);

    let map_function = |x| x * 2;

    let rdd_1_box: Box<Rdd<u32, _>> = Box::new(rdd_1);
    let rdd_map = Rdd::Map(map_function, rdd_1_box);
    print_result(&rdd_1, eval(RunOperation::Collect, &rdd_1));

    // let rdd_1_box: Box<Rdd<u32, _>> = Box::new(rdd_1);
    // let rdd_2_box: Box<Rdd<u32, _>> = Box::new(rdd_2);
    // let filter_function_1 = |x: u32| x < 5;
}

fn main() {
    println!("Corriendo ejemplos!");
    example_count();
}
