use std::process;
mod common_tp;
use common_tp::{get_flattened, read_file};
use std::rc::Rc;

#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Tipo de la funciones Map
type MapFunction<T, H> = fn(Tuple<T>) -> Tuple<H>;
type ReduceFunction<T, V> = fn(Tuple<T>) -> V;

enum Rdd<T, H, V, F = MapFunction<T, H>, RF = ReduceFunction<T, V>> {
    Cons(Vec<Tuple<T>>),
    Map(F, Rc<Rdd<T, H, V>>),
    Reduce(RF, Rc<Rdd<T, H, V>>),
}

#[derive(Debug)]
enum EvalResult<C, M, V> {
    ConsResult(Vec<Tuple<C>>),
    MapResult(Vec<Tuple<M>>),
    ReduceResult(Vec<Tuple<V>>),
}

impl<T, H, V> Rdd<T, H, V, MapFunction<T, H>, ReduceFunction<T, V>> {
    // Arma un RDD con tantas tuplas como valores tenga el arreglo
    fn new_data_from_vec(data_vector: Vec<T>) -> Self {
        let mut final_data: Vec<Tuple<T>> = Vec::new();

        // Itero por todos los valoress
        for elem in data_vector.into_iter() {
            // Por cada valor genero una tupla
            let new_tuple = Tuple::new(elem);
            final_data.push(new_tuple);
        }
        // Devuelvo el RDD con el arreglo de tuplas
        Rdd::Cons(final_data)
    }

    fn add_map(a_map_function: MapFunction<T, H>, rdd: Rdd<T, H, V, MapFunction<T, H>, ReduceFunction<T, V>>) -> Self {
        Rdd::Map(a_map_function, Rc::new(rdd))
    }

    fn add_reduce(a_reduce_function: ReduceFunction<T, V>, rdd: Rdd<T, H, V, MapFunction<T, H>, ReduceFunction<T, V>>) -> Self {
        Rdd::Reduce(a_reduce_function, Rc::new(rdd))
    }
}

fn eval<T, H, V>(rdd: &Rdd<T, H, V, MapFunction<T, H>, ReduceFunction<T, V>>) -> EvalResult<T, H, V>
where
    T: Clone + Send + Sync + Eq + std::hash::Hash + std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => EvalResult::ConsResult(data.to_vec()),
        Rdd::Map(map_function, e1) => {
            match eval(&e1) {
                EvalResult::ConsResult(data_vector) => {
                    let map_result = data_vector.into_iter()
                    .map(map_function)
                    .collect::<Vec<Tuple<_>>>();

                    EvalResult::MapResult(map_result)
                },
                _ => panic!("Not valid RDD")
            }
        },
        _ => panic!("Error. Falta implementar el Reduce")
        // Rdd::Reduce(reduce_function, e1) => {
        //     match eval(&e1) {
        //         EvalResult::ConsResult(data_vector) => {
        //             let map_result = data_vector.into_iter()
        //             .map(reduce_function)
        //             .collect::<Vec<Tuple<_>>>();

        //             EvalResult::MapResult(map_result)
        //         },
        //         _ => panic!("Not valid RDD")
        //     }
        // },
    }
}

fn main() {
    let content = read_file();
    if let Err(error) = content {
        println!("Error al leer el archivo -> {:#?}", error);
        process::exit(1);
    }

    // Obtengo y hago un flatten del contenido
    let content = content.unwrap();
    let content_flattened = get_flattened(content);
    // println!("Contenido del archivo {:#?}", content_flattened); // Imprimo el contenido del archivo

    // Creo un RDD con los valores flattened del archivo leido
    let rdd = Rdd::new_data_from_vec(content_flattened);

    // Agrego una funcion que agrega un 1 a todas las palabras
    let map_function = |tuple: Tuple<(String)>| Tuple::new((tuple.0, 1));
    let rdd_2 = Rdd::add_map(map_function, rdd);
    test_rdd(&rdd_2); // DEBUG: Imprime lo que hay en el RDD.
    
    // Evaluo el grafico de ejecucion
    let result_eval = eval(&rdd_2);
    println!("Resultado final de evaluación -> {:?}", result_eval);
}


// Imprime los datos de un RDD que es solo Cons. Sirve solo para DEBUG
fn test_rdd<T, H, V>(rdd: &Rdd<T, H, V, MapFunction<T, H>, ReduceFunction<T, V>>)
where
    T: std::fmt::Debug,
{
    match rdd {
        Rdd::Cons(data) => println!("Data del RDD -> {:?}", data),
        Rdd::Map(_, e1) => {
            println!("\tMap");
            test_rdd(&e1)
        }
        Rdd::Reduce(_, e1) => {
            println!("\tReduce");
            test_rdd(&e1)
        }
    }
}