enum FunctionTypes {
    Generic,
    Common
}

enum Message {
    Map,
    Sort,
    Stop,
    Reduce(FunctionTypes),
    ReduceByKey
}

struct Command {
    command: Message,
    address: String
}

// Se pueden definir structs con datos!
#[derive(Debug)]
enum Test {
    V1(u32, u32),
    V2(String)
}

// Incluso metodos de los mismos!
impl Test {
    fn test_action(&self) {
        println!("Llamado al metodo de un ENUM!");
    }
}

// IMPORTANTE: buen esquema para los mensajes a ejecutar
enum MessageSpark {
    Map,
    ReduceByKey,
    TOP(u64) // El comando top requiere de un argumento
}

fn send_command(comm: &Command) {
    match &comm.command {
        Message::Map => println!("Es Map!"),
        Message::Sort => println!("Es Sort!"),
        Message::Reduce(function_type) => {
            println!("Es reduce!");
            match function_type {
                FunctionTypes::Common => println!("Encima Common!"),
                FunctionTypes::Generic => println!("Encima Generic!")
            };
        },
        _ => println!("Es cualquier otra bosta")
    };
}

// Uso de if let para evitar grandes matchs
fn simple_match(x: Option<u32>) {
    if let Some(3) = x {
        println!("Es un 3!!");
    } else {
        println!("Es cualquier otra cosa");
    }
}

fn main() {
    // Genero el comando para que realice un map
    let comm = Command {
        // command: Message::Map,
        command: Message::Reduce(FunctionTypes::Generic),
        address: String::from("192.168.16.0")
    };

    // Envio el comando
    send_command(&comm);

    // Enums con tipos de datos dentro
    let a = Test::V1(1, 2);

    println!("a.0 = {:?}", a);

    a.test_action();

    // Opcionales!
    let good_number = Some(3);

    // Se requiere poner el tipo ya que Rust no lo puede inferir con el None
    let absent_number: Option<u32> = None;

    simple_match(good_number);
    simple_match(absent_number);
}