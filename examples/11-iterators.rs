fn main() {
    let v1 = vec![1, 2, 3];
    for elem in v1.iter() {
        println!("Elemento actual -> {}", elem);
    }

    // Los iteradores implementan el metodo next() que consume el ownership si no esta en un "for" loop!
    // En este caso, sum hace uso de next, consumiendo
    // todos los elementos del mismo y dejando sin pertenencia al iterador
    let iterator = v1.iter();
    let sum_res: u64 = iterator.sum();
    println!("Suma -> {}", sum_res);
    // let sum_res_again: u64 = iterator.sum(); // ERROR: iterator perdio su ownership en la linea anterior
    // Si queremos utilizar varias veces el iterador se puede usar "into_iter" o "iter_mut" (para mutables)
    
    // Funciones Map y Collect
    // NOTA: el map es lazy, solo se ejecuta si utilizo el iterador en un for
    // o en otra funcion que requiera tomar los valores. Por ejemplo, el colect
    println!("\n\nFunciones Map y Collect");
    let v2: Vec<u32> = vec![2, 3, 4, 5];
    let v2: Vec<u32> = v2.iter().map(|x| x * 2).collect();
    println!("Vector despues del map -> {:?}", v2);
    
    let only_even: Vec<u32> = (vec![1, 2, 3, 4, 5, 6]).into_iter().filter(|x| x % 2 == 0).collect();
    println!("Vector con solo los pares {:?}", only_even);
    
    // Como cambiar el valor de los elementos del arreglo
    let v3: Vec<u32> = vec![2, 3, 4, 5];
    let v3_bool: Vec<_> = v3.iter().map(|x| x % 2 == 0).collect();
    println!("Vector despues del map que cambia el tipo -> {:?}", v3_bool);
}