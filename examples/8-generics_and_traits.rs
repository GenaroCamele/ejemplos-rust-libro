// GENERICS: permite definir funciones y estructuras para varios tipos al mismo tiempo
// SIN NINGUN TIPO DE COSTO EN PERFORMANCE!!!

use std::fmt::{self, Display};

// Retorna el elemento mas largo de una lista
fn largest(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    largest
}

// Retorna el elemento mas largo de una lista
// Implementacion con Generics
fn largest_generic<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list.iter() {
        if item > largest {
            largest = item;
        }
    }

    largest
}

// Retorna una referencia &T para evitar que se requiera un elemento
// que implemente Copy o Clone. De esa manera se evita tambien la alocacion
// la heap
fn largest_generic_2<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list.iter() {
        if item > largest {
            largest = &item;
        }
    }

    &largest
}

// Los structs tambien pueden ser genericos!
struct Point<T> {
    x: T,
    y: T,
}

// Si se quieren usar distintos tipos genericos...
struct PointDiff<T, U> {
    x: T,
    y: U,
}

// Se puede aplicar a las impls
// NOTA: notese que debe especificarse despues de impl y del
// nombre de la estructura
impl<T, U> PointDiff<T, U> {
    // Notese que no hace falta poner despues del nombre de la funcion...
    fn get_x(&self) -> &T {
        &self.x
    }

    fn get_y(&self) -> &U {
        &self.y
    }
}

// Se pueden definir las implementaciones para un solo tipo!
// Ahi no es necesario especificar el tipo generico despues del impl
impl PointDiff<i32, i32> {
    fn sum_x_y(&self) -> i32 {
        self.x + self.y
    }
}

// Se puede aplicar a enums tambien
enum _Message<T> {
    Map,
    Sort,
    Stop,
    Reduce(T),
    ReduceByKey,
}

// TRAITS: son parecidas a las interfaces en otros programas. Se utiliza para definir comportamiento
// en comun entre tipos
trait Communicate {
    fn get_age(&self) -> u8;
    // Podemos definir comportamiento por defecto en
    // caso de que no lo implementen las structs
    fn talk(&self) {
        println!("Hablando!");
    }
}

struct Person {
    name: String,
    age: u8,
}

// Implemento las funciones que defini en el trait Communicate
impl Communicate for Person {
    fn talk(&self) {
        println!(
            "Hola, soy una persona, me llamo {} y tengo {} años",
            self.name,
            self.get_age()
        );
    }

    fn get_age(&self) -> u8 {
        self.age
    }
}

impl Display for Person {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

struct Pet {
    name: String,
    age: u8,
    // owner: &Person
}

// Implemento las funciones que defini en el trait Communicate
impl Communicate for Pet {
    fn talk(&self) {
        println!(
            "Hola, soy una mascota, me llamo {} y tengo {} años humanos",
            self.name,
            self.get_age()
        );
    }

    fn get_age(&self) -> u8 {
        self.age * 8 // La edad de los perros es mucho mayor
    }
}

// Puedo poner como tipo de parametro a los traits para indicar que
// solo se van a manejar structs que implementen dichos traits
fn call_communicate_methods(something: &impl Communicate) -> &impl Communicate {
    something.talk();
    something
}

// Mismo que call_communicate_methods pero con genericos (se llama Trait bound), esta forma puede
// parecer ofuscada a veces pero es conveniente cuando se tienen varios argumentos
// del mismo tipo: no se necesita repetir "impl Communicate".
// Ademas, es la unica manera de expresar que ambos parametros deben ser del MISMO tipo
fn call_communicate_methods_generics<T: Communicate>(something1: &T, something2: &T) {
    something1.talk();
    something2.talk();
}

// NOTA: se puede especificar que se recibe un parametro que implementa mas de una
// trait. Tambien es compatible con las Trait bound
fn call_communicate_methods_multi_impl(something: &(impl Communicate + Display)) {
    something.talk();
}

// NOTA: para abreviar las Trait bound se puede especificar un where en la definicion de la funcion
fn call_communicate_methods_multi_impl_2<T>(something1: &T)
where
    T: Communicate + Display,
{
    something1.talk();
}

fn main() {
    let v1 = vec![1, 2, 3, 4, 6];
    println!("El elemento más largo es {}", largest(&v1));
    
    let _p1 = Point { x: 1, y: 2 };
    let _p2 = Point { x: 'a', y: '2' };
    let p3 = PointDiff { x: 8, y: '2' }; // Los tipos pueden ser diferentes
    println!("X = {} | Y = {}", p3.get_x(), p3.get_y());
    // println!("Suma de sus partes = {}", p3.sum_x_y()); // ERROR: No es del tipo PointDiff<i32, i32>
    
    let p4 = PointDiff { x: 8, y: 2 }; // Los tipos pueden ser diferentes
    println!("Suma de sus partes = {}", p4.sum_x_y());
    
    let v2 = vec!['a', 'b', 'c', 'd', 'x'];
    println!("El elemento más largo es {}", largest_generic(&v1));
    println!("El elemento más largo es {}", largest_generic_2(&v2));

    // Instancio las estructuras y uso las implementaciones de los traits
    let person1 = Person {
        name: String::from("Genaro"),
        age: 24,
    };
    let pet1 = Pet {
        name: String::from("Firulais"),
        age: 2,
    };
    person1.talk();
    pet1.talk();

    call_communicate_methods(&person1);
    // call_communicate_methods(&p3); // ERROR: PointDiff no implementa Communicate

    // Llamo con tipo generico
    call_communicate_methods_generics(&person1, &person1);

    // Llamo con structs que immplementen Communicate y Display
    call_communicate_methods_multi_impl(&person1);
    call_communicate_methods_multi_impl_2(&person1);
}
