#[derive(Debug, Clone)]
struct Tuple<T: ?Sized>(T);

// Crea un nuevo data
impl<T> Tuple<T> {
    fn new(x: T) -> Tuple<T> {
        Tuple(x)
    }
}

// Implemento un trait para que cambie el tipo del vec
trait Operation<T> {
    fn eval(&self) -> Vec<Tuple<T>>;
}

// Implemento la operacion para la data constante
impl<T: Clone> Operation<T> for Vec<Tuple<T>> {
    fn eval(&self) -> Vec<Tuple<T>> {
        self.clone()
    }
}

// Operacion Map!
type MapOperation<O, U, T> = (O, fn(Tuple<U>) -> Tuple<T>);

impl<O, T, U> Operation<T> for MapOperation<O, U, T>
where
    O: Operation<U>,
{
    fn eval(&self) -> Vec<Tuple<T>> {
        self.0.eval().into_iter().map(self.1).collect()
    }
}

// Operacion Filter
type FilterOperation<O, U> = (O, fn(&Tuple<U>) -> bool);

impl<O, T> Operation<T> for FilterOperation<O, T>
where
    O: Operation<T>,
{
    fn eval(&self) -> Vec<Tuple<T>> {
        self.0.eval().into_iter().filter(self.1).collect()
    }
}

// Operacion Reduce
type ReduceOperation<O, T> = (O, T, fn(T, Tuple<T>) -> T);

impl<O, T> Operation<T> for ReduceOperation<O, T>
where
    O: Operation<T>,
    T: Copy,
{
    fn eval(&self) -> Vec<Tuple<T>> {
        let fold_result = self.0.eval().into_iter().fold(self.1, self.2);
        vec![Tuple::new(fold_result)]
    }
}

fn main() {
    // I define a function that will change my data
    let a_map_funcion: fn(Tuple<usize>) -> Tuple<(usize, usize, bool)> =
        |tuple| Tuple::new((tuple.0, 4, true));

    // +++++++++ MAP +++++++++
    let vec_of_tuples = vec![Tuple::new(1), Tuple::new(2), Tuple::new(3)];
    // let operation: MapOperation<Vec<Tuple<usize>, usize, (usize, usize, bool)>> = (vec_of_tuples, a_map_funcion);
    let operation = (vec_of_tuples.clone(), a_map_funcion);
    
    let result = operation.eval();
    println!("Result -> {:?}", result); // Expected [(1,3),(1,4),(1,5)]
    
    let a_map_funcion_2: fn(Tuple<(usize, usize, bool)>) -> Tuple<(bool)> =
    |tuple| Tuple::new((tuple.0).0 % 2 == 0);
    
    let operation_2 = (result, a_map_funcion_2);
    let result = operation_2.eval();
    println!("Result -> {:?}", result); // Expected [false, true, false]
    
    // +++++++++ FILTER +++++++++
    let a_filter_function: fn(&Tuple<(bool)>) -> bool = |tuple| (*tuple).0;
    let operation_3 = (result, a_filter_function);
    let result = operation_3.eval();
    println!("Result -> {:?}", result); // Expected [true]
    
    // +++++++++ REDUCE +++++++++
    let a_reduce_function: fn(usize, Tuple<usize>) -> usize = |acc, tuple| acc + tuple.0;
    let operation_4 = (vec_of_tuples.clone(), 0, a_reduce_function);
    let result = operation_4.eval();
    println!("Result -> {:?}", result); // Expected [6]
}
